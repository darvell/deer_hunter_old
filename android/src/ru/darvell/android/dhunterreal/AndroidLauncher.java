package ru.darvell.android.dhunterreal;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Gravity;
import com.android.billingclient.api.*;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidGraphics;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.games.*;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import ru.darvell.android.dhunterreal.utils.Constants;
import ru.darvell.android.dhunterreal.utils.GdxLog;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AndroidLauncher extends AndroidApplication implements ActionResolver, SkuDetailsResponseListener, PurchasesUpdatedListener {

    private static final String TAG = "AndroidLauncher";
    private static final int RC_UNUSED = 5001;
    private static final int RC_SIGN_IN = 9001;
    private static final int RC_LEADERBOARD_UI = 9004;
    private GoogleSignInClient googleSignInClient;
    private LeaderboardsClient leaderboardsClient;
    private AchievementsClient achievementsClient;
    private GoogleSignInAccount googleSignInAccount;
    private BillingClient billingClient;
    private List<MySkuDetails> mySkuDetailsList;
    private PlayersClient playersClient;
    private PurchasesListener purchasesListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBilling();
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new DHunterReal(this), config);
        googleSignInClient = GoogleSignIn.getClient(this,
                new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                        .requestEmail()
                        .build()
        );
    }

    private void initBilling() {
        billingClient = BillingClient.newBuilder(this).setListener(this).build();
        https:
//www.youtube.com/watch?v=u2Ibob2Bqqo
        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    // The billing client is ready. You can query purchases here.
                    GdxLog.d(TAG, "Billing ready");
                    requestSkuDetails();
                } else {
                    GdxLog.d(TAG, "Billing NOT ready " + billingResponseCode);
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
            }
        });
    }

    private void requestSkuDetails() {
        List skuList = new ArrayList<>();
        skuList.add(Constants.SKU_BEER);
        skuList.add(Constants.SKU_SALAD);
        skuList.add(Constants.SKU_MEAT);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
        billingClient.querySkuDetailsAsync(params.build(), this);
    }

    private void startSignInIntent() {
        Log.d(TAG, "startSignInIntent");
        startActivityForResult(googleSignInClient.getSignInIntent(), RC_SIGN_IN);
    }

    @Override
    public void onSignInButtonClicked() {
        startSignInIntent();
    }

    @Override
    public void onSignOutButtonClicked() {
        Log.d(TAG, "signOut()");

        if (!isSignedIn()) {
            Log.w(TAG, "signOut() called, but was not signed in!");
            return;
        }

        googleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        boolean successful = task.isSuccessful();
                        Log.d(TAG, "signOut(): " + (successful ? "success" : "failed"));

                        onDisconnected();
                    }
                });
    }

    @Override
    public boolean isSignedIn() {
        Log.d(TAG, "isSignedIn= " + (GoogleSignIn.getLastSignedInAccount(this) != null));
        return GoogleSignIn.getLastSignedInAccount(this) != null;
    }

    @Override
    public void signInSilently() {
        Log.d(TAG, "signInSilently()");

        googleSignInClient.silentSignIn().addOnCompleteListener(this,
                new OnCompleteListener<GoogleSignInAccount>() {
                    @Override
                    public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInSilently(): success");
                            onConnected(task.getResult());
                        } else {
                            Log.d(TAG, "signInSilently(): failure", task.getException());
                            onDisconnected();
                        }
                    }
                });

    }

    @Override
    public void submitScore(String leaderboardId, int highScore) {
        Log.d(TAG, "leaderboardId IN= " + leaderboardId);
        if (isSignedIn()) {
            Log.d(TAG, "leaderboardId OUT= " + leaderboardId);
            leaderboardsClient.submitScore(leaderboardId, highScore);
        }
    }

    @Override
    public void showLeaderboard(String leaderboardId) {
        Log.d(TAG, "showLeaderboard IN= " + leaderboardId);
        if (isSignedIn()) {
            Log.d(TAG, "showLeaderboard OUT= " + leaderboardId);
            Games.getLeaderboardsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                    .getLeaderboardIntent(leaderboardId)
                    .addOnSuccessListener(new OnSuccessListener<Intent>() {
                        @Override
                        public void onSuccess(Intent intent) {
                            startActivityForResult(intent, RC_LEADERBOARD_UI);
                        }
                    });
        }
    }

    @Override
    public void setTrackerScreenName(String screenName) {

    }

    @Override
    public void unlockAchievement(String achievmentId) {
        Log.d(TAG, "achievmentId IN= " + achievmentId);
        if (isSignedIn()) {
            achievementsClient.unlock(achievmentId);
        }
    }

    @Override
    public List<MySkuDetails> getSkus() {
        return mySkuDetailsList;
    }

    @Override
    public void purchaseItem(String skuId) {
        BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                .setSku(skuId)
                .setType(BillingClient.SkuType.INAPP) // SkuType.SUB for subscription
                .build();
        int responseCode = billingClient.launchBillingFlow(this, flowParams);
    }

    @Override
    public void setupListener(PurchasesListener purchasesListener) {
        this.purchasesListener = purchasesListener;
    }

    @Override
    public void disableListener() {
        this.purchasesListener = null;
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
//            updateUI(account);
            Log.d(TAG, "Signed in successfully");
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            Log.d(TAG, e.getMessage());
//            updateUI(null);
        }
    }

    private void onConnected(final GoogleSignInAccount googleSignInAccount) {
        Log.d(TAG, "onConnected(): connected to Google APIs");

        this.googleSignInAccount = googleSignInAccount;

        leaderboardsClient = Games.getLeaderboardsClient(this, googleSignInAccount);
        playersClient = Games.getPlayersClient(this, googleSignInAccount);
        achievementsClient = Games.getAchievementsClient(this, googleSignInAccount);

        // Set the greetingSigned in successfully appropriately on main menu
        playersClient.getCurrentPlayer()
                .addOnCompleteListener(new OnCompleteListener<Player>() {
                    @Override
                    public void onComplete(@NonNull Task<Player> task) {
                        String displayName;
                        if (task.isSuccessful()) {
                            displayName = task.getResult().getDisplayName();
                        } else {
                            Exception e = task.getException();
//                            handleException(e, getString(R.string.players_exception));
                            displayName = "???";
                        }
                        GamesClient gamesClient = Games.getGamesClient(AndroidLauncher.this, googleSignInAccount); //setGravityForPopups(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                        gamesClient.setGravityForPopups(Gravity.TOP | Gravity.CENTER_HORIZONTAL);
                        gamesClient.setViewForPopups(((AndroidGraphics) AndroidLauncher.this.getGraphics()).getView());
                    }
                });
    }

    private void onDisconnected() {
        Log.d(TAG, "onDisconnected()");

        leaderboardsClient = null;
        playersClient = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        signInSilently();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(intent);

            handleSignInResult(task);

//            try {
//                GoogleSignInAccount account = task.getResult(ApiException.class);
//                onConnected(account);
//            } catch (ApiException apiException) {
//                int statusCode = apiException.getStatusCode();
//
//
//                onDisconnected();
//
//                new AlertDialog.Builder(this)
//                        .setMessage(statusCode)
//                        .setNeutralButton(android.R.string.ok, null)
//                        .show();
//            }
        }
    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
        if (purchases != null) {
            List<String> result = new LinkedList<>();
            for (Purchase purchase : purchases) {
                result.add(purchase.getPackageName());
            }
            if (purchasesListener != null) {
                purchasesListener.onGetPurchases(result);
            }
        }
    }

    @Override
    public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
        if (responseCode == BillingClient.BillingResponse.OK) {
            mySkuDetailsList = new LinkedList<>();
            for (SkuDetails skuDetails : skuDetailsList) {

                MySkuDetails mySkuDetails = new MySkuDetails(skuDetails.getSku(), skuDetails.getPrice());
                mySkuDetailsList.add(mySkuDetails);

                GdxLog.d(TAG, skuDetails.getSku() + "  " + skuDetails.getTitle() + "  " + skuDetails.getPrice());
            }
        } else {
            GdxLog.d(TAG, "Billing response is :" + responseCode);
        }
    }

}
