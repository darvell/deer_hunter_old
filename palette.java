
import java.awt.Color;
import java.util.Hashtable;

// Generated with GIMP palette Export 
// Based on the palette hotdog
public class hotdog {

    Hashtable<String, Color> colors;

    public hotdog() {
        colors = new Hashtable<String,Color>();
        colors.put("Без имени", new Color(96, 130, 75));
        colors.put("Без имени", new Color(100, 145, 70));
        colors.put("Без имени", new Color(115, 172, 66));
        colors.put("Без имени", new Color(130, 194, 58));
        colors.put("Без имени", new Color(168, 224, 56));
        colors.put("Без имени", new Color(184, 174, 119));
        colors.put("Без имени", new Color(199, 184, 109));
        colors.put("Без имени", new Color(222, 194, 101));
        colors.put("Без имени", new Color(245, 198, 88));
        colors.put("Без имени", new Color(255, 179, 79));
        colors.put("Без имени", new Color(145, 95, 68));
        colors.put("Без имени", new Color(161, 93, 59));
        colors.put("Без имени", new Color(183, 86, 50));
        colors.put("Без имени", new Color(207, 71, 37));
        colors.put("Без имени", new Color(237, 45, 31));
        colors.put("Без имени", new Color(166, 84, 63));
        colors.put("Без имени", new Color(181, 75, 51));
        colors.put("Без имени", new Color(203, 56, 37));
        colors.put("Без имени", new Color(227, 31, 20));
        colors.put("Без имени", new Color(252, 10, 22));
        colors.put("Без имени", new Color(79, 62, 36));
        colors.put("Без имени", new Color(94, 73, 41));
        colors.put("Без имени", new Color(118, 80, 39));
        colors.put("Без имени", new Color(140, 83, 34));
        colors.put("Без имени", new Color(171, 88, 32));
        colors.put("Без имени", new Color(64, 46, 33));
        colors.put("Без имени", new Color(79, 52, 33));
        colors.put("Без имени", new Color(102, 56, 33));
        colors.put("Без имени", new Color(125, 54, 29));
        colors.put("Без имени", new Color(156, 54, 28));
    }
}