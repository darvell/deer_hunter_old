package ru.darvell.android.dhunterreal;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import ru.darvell.android.dhunterreal.screens.CatScreen;
import ru.darvell.android.dhunterreal.screens.CreditsScreen;
import ru.darvell.android.dhunterreal.screens.GameScreen;
import ru.darvell.android.dhunterreal.screens.TitleScreen;
import ru.darvell.android.dhunterreal.utils.*;

public class DHunterReal extends Game implements ChangeScreenListener {

    private static final String TAG = "DHunterReal";
    public static float MAX_Y;
    public static float MIN_Y;
    private ActionResolver actionResolver;
    private MusicPlayer musicPlayer;
    private SoundPlayer soundPlayer;

    private Screen currentScreen;

    public DHunterReal(ActionResolver actionResolver) {

        GdxLog.d(TAG, "create()");
        this.actionResolver = actionResolver;
        GdxLog.DEBUG = true;

    }


    @Override
    public void setPlayScreen() {
        currentScreen = new GameScreen(this, actionResolver, musicPlayer, soundPlayer);
        setScreen(currentScreen);
    }

    @Override
    public void setTitleScreen() {
        currentScreen = new TitleScreen(this, actionResolver, musicPlayer, soundPlayer);
        setScreen(currentScreen);
    }

    @Override
    public void setCreditsScreen() {
        currentScreen = new CreditsScreen(this, actionResolver, musicPlayer, soundPlayer);
        setScreen(currentScreen);
    }

    @Override
    public void setCatScreen() {
        currentScreen = new CatScreen(this, actionResolver, musicPlayer, soundPlayer);
        setScreen(currentScreen);
    }

    @Override
    public void create() {
        if (actionResolver != null) {
            if (!actionResolver.isSignedIn()) {
                actionResolver.onSignInButtonClicked();
            }
        }
        Assets.load();
        Assets.finishLoading();
//        doResize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        musicPlayer = new MusicPlayer();
        soundPlayer = new SoundPlayer();
        prepareToWork();
//        initPaySystem();
//        setCatScreen();
        setTitleScreen();
//        setPlayScreen();
//        setScreen(new GameScreen());
    }

    @Override
    public void dispose() {
        GdxLog.d(TAG, "dispose()");
        super.dispose();
    }

    @Override
    public void pause() {
        GdxLog.d(TAG, "pause()");
        super.pause();
    }

    @Override
    public void resume() {
        GdxLog.d(TAG, "resume()");
//        Assets.load();
//        Assets.finishLoading();
//        SoundPlayer.reload();
        super.resume();
    }

    @Override
    public void resize(int width, int height) {
       doResize(width, height);
    }

    public void prepareToWork() {
        Preferences prefs = Gdx.app.getPreferences(Constants.PREF_NAME);

        GdxLog.d(TAG, "PREF_NOT_FIRST_LAUNCH: " + prefs.getBoolean(Constants.PREF_NOT_FIRST_LAUNCH));
        GdxLog.d(TAG, "PREF_MUSIC_ENABLED: " + prefs.getBoolean(Constants.PREF_MUSIC_ENABLED));

        if (prefs.getBoolean(Constants.PREF_NOT_FIRST_LAUNCH)) {
            musicPlayer.setEnabled(prefs.getBoolean(Constants.PREF_MUSIC_ENABLED));
        } else {
            musicPlayer.setEnabled(true);
            prefs.putBoolean(Constants.PREF_NOT_FIRST_LAUNCH, true);
            prefs.flush();
        }
    }

    private void doResize(int width, int height) {
        super.resize(width, height);

        float aspectRatio = (float) height / width;
        float viewportHeight = Constants.APP_WIDTH * aspectRatio;

        MIN_Y = (Constants.APP_HEIGHT - viewportHeight) / 2;
        MAX_Y = MIN_Y + viewportHeight;

        GdxLog.d(TAG, "AspectRatio " + aspectRatio);
        GdxLog.d(TAG, "VieportHeight " + viewportHeight);

        currentScreen.resize(width, height);
    }


}
