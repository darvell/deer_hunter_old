package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.darvell.android.dhunterreal.actors.interfaces.MusicBirdListener;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;

import static com.badlogic.gdx.graphics.g2d.Animation.PlayMode.LOOP_RANDOM;

public class MusicBird extends Actor {

    private Animation stayAnimation;
    private Animation singAnimation;
    private MusicBirdListener listener;

    private boolean playback;

    private float stateTime = 0f;

    public MusicBird(MusicBirdListener listener) {
        this.listener = listener;
        initTouch();
        defineDemens();
        initAnimation();
    }

    private void initTouch() {
        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                setPlayback(!isPlayback());
                listener.musicBirdTouched();
                return true;
            }
        });
    }

    private void defineDemens() {
        setX(Constants.MUSICBIRD_START_POSITION.x);
        setY(Constants.MUSICBIRD_START_POSITION.y);
        setWidth(32 * 2);
        setHeight(32 * 2);
    }

    private void initAnimation() {
        singAnimation = Assets.getAnimation(Assets.MUSIC_BIRD_SING, 0.8f);
        stayAnimation = Assets.getAnimation(Assets.MUSIC_BIRD_STAY, 1f);

        singAnimation.setPlayMode(LOOP_RANDOM);
        stayAnimation.setPlayMode(LOOP_RANDOM);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        stateTime += Gdx.graphics.getDeltaTime();
        if (isPlayback()) {
            batch.draw((TextureRegion) singAnimation.getKeyFrame(stateTime), getX(), getY(), getWidth(), getHeight());
        } else {
            batch.draw((TextureRegion) stayAnimation.getKeyFrame(stateTime), getX(), getY(), getWidth(), getHeight());
        }
    }

    public boolean isPlayback() {
        return playback;
    }

    public void setPlayback(boolean playback) {
        this.playback = playback;
    }
}
