package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.android.dhunterreal.actors.interfaces.AnyBoodies;
import ru.darvell.android.dhunterreal.box2d.FallenBirdUserData;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;

import static com.badlogic.gdx.graphics.g2d.Animation.PlayMode.LOOP;

public class FallenLifeBird extends Person implements AnyBoodies {

    private Animation normalAnimation;
    private Animation flyAnimation;

    private boolean needCountLife = false;
    private float lifeTime = 0f;

    public FallenLifeBird(Body body) {
        super(body);
        sizeWidth = 32;
        sizeHeight = 32;
        flyAnimation = Assets.getAnimation(Assets.LIFEBIRD_RED_FLY, 0.1f);
        flyAnimation.setPlayMode(LOOP);
    }


    @Override
    public void draw(Batch batch, float a) {
        super.draw(batch, a);
        if (body != null) {

//            Body body = getBody();
//
//            batch.draw(Assets.getTexture(Assets.TEST_SQUARE), transformToScreen(body.getPosition().x - getUserData().getWidth() / 2),
//                    transformToScreen(body.getPosition().y - getUserData().getWidth() / 2),
//                    transformToScreen(getUserData().getWidth()),
//                    transformToScreen(getUserData().getHeight())
//            );

            stateTime += Gdx.graphics.getDeltaTime();
            batch.draw((TextureRegion) flyAnimation.getKeyFrame(stateTime, true), getXForDraw(), getYForDraw(), getWidth(), getHeight());
        }
    }

    @Override
    public FallenBirdUserData getUserData() throws NullPointerException {
        return (FallenBirdUserData) body.getUserData();
    }

    private float getXForDraw() {
        return getX() - transformToScreen(getUserData().getWidth() / 2) + 10;

    }

    private float getYForDraw() {
        return getY() - transformToScreen(getUserData().getHeight() / 2) + 7;
    }

    public void onGround(){
        lifeTime = Constants.LIFRBIRD_LIFE_TIME;
        needCountLife = true;
    }

    public void update(float delta){
        if (needCountLife){
            lifeTime -= delta;
            if (lifeTime <= 0){
                if (body != null) {
                    getUserData().setNeedDelete(true);
                }
            }
        }
    }

    @Override
    public void setNeedDelete(boolean b) {
        getUserData().setNeedDelete(b);
    }
}
