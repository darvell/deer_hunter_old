package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;

public class DeadBody extends Actor {

    private TextureRegion textureRegionRigth;
    private TextureRegion textureRegionLeft;
    private Direction direction;

    public DeadBody(float x, float y, Direction direction) {
        textureRegionRigth = Assets.getTexture(Assets.DEER_DEAD_RIGHT);
        textureRegionLeft = Assets.getTexture(Assets.DEER_DEAD_LEFT);
        setX(x);
        setY(y);
        setWidth(Constants.ENEMY_DRAW_WIDTH);
        setHeight(Constants.ENEMY_DRAW_HEIGHT);
        this.direction = direction;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        TextureRegion textureToDraw = null;
        switch (direction) {
            case LEFT:
                textureToDraw = textureRegionLeft;
                break;
            case RIGHT:
                textureToDraw = textureRegionRigth;
                break;
        }
        batch.draw(textureToDraw, getX(), getY(), getWidth(), getHeight());
    }
}
