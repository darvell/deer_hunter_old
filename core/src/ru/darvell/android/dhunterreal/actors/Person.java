package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.physics.box2d.Body;

public abstract class Person extends GameActor {

    protected Animation walkRightAnimation;
    protected Animation walkLeftAnimation;
    protected Animation stayRightAnimation;
    protected Animation stayLeftAnimation;
    protected Animation jumpRightAnimation;
    protected Animation jumpLeftAnimation;
    protected Animation shutRightAnimation;
    protected Animation shutLeftAnimation;
    protected float stateTime;

    public Person(Body body) {
        super(body);
    }
}
