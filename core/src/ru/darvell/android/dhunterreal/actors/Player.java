package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.android.dhunterreal.box2d.PlayerUserData;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;
import ru.darvell.android.dhunterreal.utils.SoundPlayer;

public class Player extends Person {

    private boolean shutting;
    private boolean needCreateBullet;
    private boolean hasShoot;
    private boolean inGodMode;
    private boolean badJump;

    private float godModeTimeLeft = 0f;

    private int lifes;

    private Direction direction;

    private Vector2 velocity = new Vector2();


    private Animation badLeftJumpAnimation;
    private Animation badRightJumpAnimation;
    private SoundPlayer soundPlayer;

    public Player(Body body, SoundPlayer soundPlayer) {
        super(body);
        this.soundPlayer = soundPlayer;
        direction = Direction.LEFT;
        walkRightAnimation = Assets.getAnimation(Assets.FRAY_WALK_RIGHT, 0.1f);
        walkLeftAnimation = Assets.getAnimation(Assets.FRAY_WALK_LEFT, 0.1f);
//        stayLeftAnimation = Assets.getAnimation(Assets.DEER_WALK_LEFT, 0.1f);
        stayRightAnimation = Assets.getAnimation(Assets.FRAY_STAY_RIGHT, 0.1f);
        stayLeftAnimation = Assets.getAnimation(Assets.FRAY_STAY_LEFT, 0.1f);
        jumpRightAnimation = Assets.getAnimation(Assets.FRAY_JUMP_RIGHT, 0.1f);
        jumpLeftAnimation = Assets.getAnimation(Assets.FRAY_JUMP_LEFT, 0.1f);
        shutRightAnimation = Assets.getAnimation(Assets.FRAY_SHUT_RIGHT, 0.4f);
        shutLeftAnimation = Assets.getAnimation(Assets.FRAY_SHUT_LEFT, 0.4f);
        badLeftJumpAnimation = Assets.getAnimation(Assets.FRAY_BAD_JUMP_LEFT, 0.1f);
        badRightJumpAnimation = Assets.getAnimation(Assets.FRAY_BAD_JUMP_RIGHT, 0.1f);

        lifes = Constants.START_LIVES_COUNT;

        stateTime = 0f;
    }

    @Override
    public PlayerUserData getUserData() {
        return (PlayerUserData) userData;
    }

    public void jump() {
        if (body != null) {
            if (!isJumping()) {
                body.applyLinearImpulse(getUserData().getJumpingLinearImpulse(), body.getWorldCenter(), true);
                setJumping(true);
                resetFriction();
                soundPlayer.playJumpPlayer();
            }
        }
    }


    public void initMoveRight() {
        setMoving(true);
        direction = Direction.RIGHT;
        velocity.x = getUserData().getSpeed();
    }

    public void initMoveLeft() {
        setMoving(true);
        direction = Direction.LEFT;
        velocity.x = -getUserData().getSpeed();
    }


    public void update(float delta) {
        if (body != null) {
            Vector2 tmpVel = body.getLinearVelocity();
            velocity.y = tmpVel.y;
            body.setLinearVelocity(velocity);
        }
        if (isInGodMode()) {
            godModeTimeLeft += delta;
        }
        if (godModeTimeLeft > Constants.RUNNER_MAX_GOD_MODE_TIME) {
            inGodMode = false;
            godModeTimeLeft = 0;
        }
    }

    public void stopMove() {
        setMoving(false);
        velocity.x = 0;
        velocity.y = 0;
    }

    public void stopMoveLeft() {
        if (direction.equals(Direction.LEFT)) {
            setMoving(false);
            velocity.x = 0;
        }

//        velocity.y = 0;
    }

    public void stopMoveRight() {
        if (direction.equals(Direction.RIGHT)) {
            setMoving(false);
            velocity.x = 0;
        }
        velocity.y = 0;
    }

    private void setFriction() {
        if (body != null) {
            getUserData().getBodyFixture().setFriction(getUserData().getFriction());
        }
    }

    private void resetFriction() {
        if (body != null) {
            getUserData().getBodyFixture().setFriction(0);
        }
    }

    public void landed() {
        soundPlayer.playLanding();
        setJumping(false);
        badJump = false;
        setFriction();
    }

    public void hit() {
        if (body != null) {
            body.applyLinearImpulse(new Vector2(0f, 1.5f), body.getWorldCenter(), true);
            setJumping(true);
        }
        lifes -= 1;
        badJump = true;
        if (lifes > 0) {
            inGodMode = true;
        }
    }

    public Direction getDirection() {
        return direction;
    }

    public Vector2 getPosition() {
        if (body != null) {
            return body.getPosition();
        } else {
            return null;
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        stateTime += Gdx.graphics.getDeltaTime();
        if (shutting) {
            if (direction.equals(Direction.LEFT)) {
                batch.draw((TextureRegion) shutLeftAnimation.getKeyFrame(stateTime, false), getXForDraw(), getY(), getWidth(), getHeight());
                if ((shutLeftAnimation.getKeyFrameIndex(stateTime) == 2) && (!hasShoot)) {
                    needCreateBullet = true;
                    hasShoot = true;
                }
                if (shutRightAnimation.isAnimationFinished(stateTime)) {
                    resetShutting();
                }
            } else {
                batch.draw((TextureRegion) shutRightAnimation.getKeyFrame(stateTime, false), getXForDraw(), getY(), getWidth(), getHeight());
                if ((shutRightAnimation.getKeyFrameIndex(stateTime) == 2) && (!hasShoot)) {
                    needCreateBullet = true;
                    hasShoot = true;
                }
                if (shutRightAnimation.isAnimationFinished(stateTime)) {
                    resetShutting();
                }
            }

        } else if (isJumping()) {
            if (direction.equals(Direction.LEFT)) {
                if (badJump) {
                    batch.draw((TextureRegion) badLeftJumpAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
                } else {
                    batch.draw((TextureRegion) jumpLeftAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
                }
            } else {
                if (badJump) {
                    batch.draw((TextureRegion) badRightJumpAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
                } else {
                    batch.draw((TextureRegion) jumpRightAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
                }
            }

        } else if (isMoving()) {
            if (direction.equals(Direction.RIGHT)) {
                batch.draw((TextureRegion) walkRightAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
            } else {
                batch.draw((TextureRegion) walkLeftAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
            }
        } else {
            if (direction.equals(Direction.RIGHT)) {
                batch.draw((TextureRegion) stayRightAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
            } else {
                batch.draw((TextureRegion) stayLeftAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
            }
        }

//        Body body = getUserData().getBodyFixture().getBody();
//
//        batch.draw(Assets.getTexture(Assets.TEST_SQUARE), transformToScreen(body.getPosition().x - getUserData().getWidth()/2),
//                transformToScreen(body.getPosition().y - getUserData().getWidth() / 2),
//                transformToScreen(getUserData().getWidth()),
//                transformToScreen(getUserData().getHeight())
//        );


    }

    public boolean isShutting() {
        return shutting;
    }

    public void setShutting(boolean shutting) {
        if (!isShutting()) {
            if (shutting) {
                stateTime = 0;
                this.shutting = shutting;
                hasShoot = false;
            }
        }
    }

    public boolean isNeedCreateBullet() {
        if (needCreateBullet) {
            needCreateBullet = false;
            return true;
        } else {
            return false;
        }
    }

    public void resetShutting() {
        shutting = false;
        stateTime = 0;
    }

    public boolean isDead() {
        return lifes <= 0;
    }

    public void setLifes(int lifes) {
        this.lifes = lifes;
    }

    public boolean isInGodMode() {
        return inGodMode;
    }

    public boolean isBadJump() {
        return badJump;
    }

    public void setBadJump(boolean badJump) {
        this.badJump = badJump;
    }

    private float getXForDraw() {
        return direction.equals(Direction.RIGHT) ?
                (getX() - transformToScreen(getUserData().getWidth() / 2) + 10)
                : (getX() - transformToScreen(getUserData().getWidth() / 2));
    }

    public void reset(){
        lifes = Constants.START_LIVES_COUNT;
    }

    public int getLifes() {
        return lifes;
    }

    public boolean catchBird(){
        if (lifes == 1){
            lifes++;
            return true;
        } else {
            return false;
        }
    }
}
