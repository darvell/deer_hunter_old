package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.darvell.android.dhunterreal.BackgroundClickListener;
import ru.darvell.android.dhunterreal.utils.Constants;

public class Background extends Actor{

    private TextureRegion texture;

    public Background(float x, float y, TextureRegion texture) {
        this.texture = texture;
        setX(x);
        setY(y);
        setWidth(Constants.APP_WIDTH);
        setHeight(Constants.APP_HEIGHT);
    }

    public Background(float x, float y, TextureRegion texture, final BackgroundClickListener listener) {
        this(x, y, texture);
        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                listener.tochDownBackground();
                return true;
            }
        });
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        batch.draw(texture, getX(), getY(), Constants.APP_WIDTH, Constants.APP_HEIGHT);
    }
}
