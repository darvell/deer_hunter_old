package ru.darvell.android.dhunterreal.actors.interfaces;

public interface MusicBirdListener {
    void musicBirdTouched();
}
