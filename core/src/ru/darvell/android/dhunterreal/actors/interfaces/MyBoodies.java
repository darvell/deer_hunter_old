package ru.darvell.android.dhunterreal.actors.interfaces;

public interface MyBoodies {
    void setNeedDelete(boolean b);
    void update(float delta);
}
