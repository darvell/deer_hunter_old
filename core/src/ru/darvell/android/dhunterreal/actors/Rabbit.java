package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.android.dhunterreal.actors.interfaces.Enemy;
import ru.darvell.android.dhunterreal.box2d.RabbitUserData;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;
import ru.darvell.android.dhunterreal.utils.SoundPlayer;

public class Rabbit extends Person implements Enemy {

    private SoundPlayer soundPlayer;
    private float waitingTime;
    private float waitingTimeSpend = 0f;

    public Rabbit(Body body, SoundPlayer soundPlayer) {
        super(body);
        sizeWidth = 32f;
        sizeHeight = 32f;
        this.soundPlayer = soundPlayer;
        stayLeftAnimation = Assets.getAnimation(Assets.RABBIT_STAY_LEFT, 0.5f);
        stayRightAnimation = Assets.getAnimation(Assets.RABBIT_STAY_RIGHT, 0.5f);
        jumpRightAnimation = Assets.getAnimation(Assets.RABBIT_JUMP_RIGHT, 0.5f);
        jumpLeftAnimation = Assets.getAnimation(Assets.RABBIT_JUMP_LEFT, 0.5f);
        stayLeftAnimation.setPlayMode(Animation.PlayMode.LOOP_REVERSED);
        stayRightAnimation.setPlayMode(Animation.PlayMode.LOOP_REVERSED);
        jumpRightAnimation.setPlayMode(Animation.PlayMode.LOOP_REVERSED);
        jumpLeftAnimation.setPlayMode(Animation.PlayMode.LOOP_REVERSED);
        waitingTime = Constants.RABBIT_WAITING_TIME;
    }

    @Override
    public RabbitUserData getUserData() throws NullPointerException {
        return (RabbitUserData) this.userData;
    }

    public Direction getDirection() {
        return getUserData().getDirection();
    }

    public void setDirection(Direction direction) {
        getUserData().setDirection(direction);
    }

    @Override
    public void draw(Batch batch, float a) {
        super.draw(batch, a);
//        Body body = getBody();
//
//        batch.draw(Assets.getTexture(Assets.TEST_SQUARE), transformToScreen(body.getPosition().x - getUserData().getWidth()/2),
//                transformToScreen(body.getPosition().y - getUserData().getWidth() / 2),
//                transformToScreen(getUserData().getWidth()),
//                transformToScreen(getUserData().getHeight())
//        );

        stateTime += Gdx.graphics.getDeltaTime();

        TextureRegion tr;

        if (getUserData().isLanded()){
            if (getUserData().getDirection().equals(Direction.LEFT)){
                tr = (TextureRegion) stayLeftAnimation.getKeyFrame(stateTime);
            } else {
                tr = (TextureRegion) stayRightAnimation.getKeyFrame(stateTime);
            }
        } else {
            if (getUserData().getDirection().equals(Direction.LEFT)){
                tr = (TextureRegion) jumpLeftAnimation.getKeyFrame(stateTime);
            } else {
                tr = (TextureRegion) jumpRightAnimation.getKeyFrame(stateTime);
            }
        }
        batch.draw(tr, getXForDraw(), getYForDraw(), getWidth(), getHeight());
    }

    private float getXForDraw() {
        return getUserData().getDirection().equals(Direction.RIGHT) ?
                (getX() - transformToScreen(getUserData().getWidth() / 2))
                : (getX() - transformToScreen(getUserData().getWidth() / 2));
    }

    private float getYForDraw() {
        return getY();
    }

    public void resetMoveDirection() {
        getUserData().setNeedChangeDirection(false);
        if (getDirection().equals(Direction.RIGHT)) {
            setDirection(Direction.LEFT);
        } else {
            setDirection(Direction.RIGHT);
        }
    }

    @Override
    public void setNeedDelete(boolean b) {
        getUserData().setNeedDelete(true);
    }

    @Override
    public void update(float delta) {
        if (getUserData().isNeedChangeDirection()){
            resetMoveDirection();
        }
        if (getBody() != null){
            if (getUserData().isLanded()){
                waitingTimeSpend += delta;
                if (waitingTimeSpend > waitingTime) {
                    soundPlayer.playRabbitJump();
                    body.applyLinearImpulse(getUserData().getJumpingLinearImpulse(), body.getWorldCenter(), true);
                    getUserData().setLanded(false);
                    waitingTimeSpend = 0;
                }
            }
        }
    }
}
