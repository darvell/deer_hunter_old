package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;

import static com.badlogic.gdx.graphics.g2d.Animation.PlayMode.LOOP;
import static com.badlogic.gdx.graphics.g2d.Animation.PlayMode.LOOP_RANDOM;


public class LifeBird extends Actor {

    private Animation normalAnimation;
    private Animation flyAnimation;

    private float stateTime = 0f;

    private boolean moved = false;


    Vector2 position = new Vector2();
    Vector2 velocity = new Vector2(100f, 200f);
    Vector2 movement = new Vector2();


    public LifeBird(){
        setStartPosition();
        setWidth(32 * 2);
        setHeight(32 * 2);
        normalAnimation = Assets.getAnimation(Assets.LIFEBIRD_RED_STAY, 1f);
        normalAnimation.setPlayMode(LOOP_RANDOM);

        flyAnimation = Assets.getAnimation(Assets.LIFEBIRD_RED_FLY, 0.1f);
        flyAnimation.setPlayMode(LOOP);
    }

    private void setStartPosition(){
        setX(Constants.LIFEBIRD_START_POSITION.x);
        setY(Constants.LIFEBIRD_START_POSITION.y);
        position.set(Constants.LIFEBIRD_START_POSITION);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        stateTime += Gdx.graphics.getDeltaTime();
        if (moved){
            batch.draw((TextureRegion) flyAnimation.getKeyFrame(stateTime), getX(), getY(), getWidth(), getHeight());
        } else {
            batch.draw((TextureRegion) normalAnimation.getKeyFrame(stateTime), getX(), getY(), getWidth(), getHeight());
        }
    }

    public void flyAway(){
        moved = true;
    }



    @Override
    public void act(float delta) {
        super.act(delta);
        if (moved) {
            movement.set(velocity).scl(delta);
            position.add(movement);
            if (position.y > Constants.APP_HEIGHT){
                moved = false;
            }
        }

        setX(position.x);
        setY(position.y);
    }

    public void reset(){
        moved = false;
        setStartPosition();

    }




}
