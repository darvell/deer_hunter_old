package ru.darvell.android.dhunterreal.actors.ai;

import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import ru.darvell.android.dhunterreal.actors.ui.Cat;

public enum CatState implements State<Cat> {
    BLINKING;

    @Override
    public void enter(Cat entity) {

    }

    @Override
    public void update(Cat entity) {

    }

    @Override
    public void exit(Cat entity) {

    }

    @Override
    public boolean onMessage(Cat entity, Telegram telegram) {
        return false;
    }
}
