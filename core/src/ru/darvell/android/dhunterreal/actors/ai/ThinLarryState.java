package ru.darvell.android.dhunterreal.actors.ai;

import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.Telegram;
import ru.darvell.android.dhunterreal.actors.ThinLarry;
import ru.darvell.android.dhunterreal.utils.DiceUtils;

public enum ThinLarryState implements State<ThinLarry> {

    WALK() {
        @Override
        public void update(ThinLarry larry) {
            if (larry.isNeedChangeDirection()){
                larry.stopMove();
                larry.resetMoveDirection();
                larry.prepareCharge();
                larry.stateMachine.changeState(CHARGE);
            } else if (larry.isCenterReached() || (DiceUtils.throwDices(10, larry.getWinDiceCount()))){
                larry.setCenterReached(false);
                larry.stopMove();
                larry.prepareOpenMoth();
                larry.stateMachine.changeState(OPEN_MOTH);
            }
        }
    },

    CHARGE() {
        @Override
        public void update(ThinLarry larry) {
            if (larry.getChargingTime() < 0){
                larry.resetChargingTime();
                larry.setNeedPoisonBlast(true);
                larry.preparePause();
                larry.stateMachine.changeState(BLASTING);
            }
        }
    },

    BLASTING(){
        @Override
        public void update(ThinLarry larry) {
            if (larry.getPauseTime() < 0){
                larry.initMoving();
                larry.resetPauseTime();
                larry.stateMachine.changeState(WALK);
            }
        }
    },

    OPEN_MOTH(){
        @Override
        public void update(ThinLarry larry) {
            if (larry.getOpenMothTime()<0){
                larry.resetOpenMothTime();
                larry.preparePause();
                larry.setNeedCenterPoisonBlast(true);
                larry.stateMachine.changeState(BLASTING_CENTER);
            }
        }
    },

    BLASTING_CENTER(){
        @Override
        public void update(ThinLarry larry) {
            if (larry.getPauseTime() < 0){
                larry.resetPauseTime();
                larry.initMoving();
                larry.stateMachine.changeState(WALK);
            }
        }
    }

    ;


    @Override
    public void enter(ThinLarry larry) {

    }

    @Override
    public void exit(ThinLarry larry) {

    }

    @Override
    public boolean onMessage(ThinLarry entity, Telegram telegram) {
        return false;
    }
}
