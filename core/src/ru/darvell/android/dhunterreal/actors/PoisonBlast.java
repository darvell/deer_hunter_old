package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.android.dhunterreal.actors.interfaces.AnyBoodies;
import ru.darvell.android.dhunterreal.box2d.PoisonBlastUserData;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.utils.Assets;

public class PoisonBlast extends Person implements AnyBoodies {

    private float timeToDelete = 3f;

    public PoisonBlast(Body body, boolean randomDirection) {
        super(body);
        sizeWidth = 18f;
        sizeHeight = 18;
        walkLeftAnimation = Assets.getAnimation(Assets.POISON_BLAST_NORMAL, 0.03f);
        stayLeftAnimation = Assets.getAnimation(Assets.POISON_BLAST_GROUND, 0.1f);
        shoot(randomDirection);
    }

    private void shoot(boolean randomDirection) {
        float rndX = MathUtils.random(0.1f, 0.51f);
        if (randomDirection){setupRandomDirection();}
        if (getUserData().getDirection().equals(Direction.LEFT)) {
            rndX *= -1;
        }

        float rndY = MathUtils.random(0.1f, 0.51f);
        body.applyLinearImpulse(new Vector2(rndX, rndY), body.getWorldCenter(), true);
    }


    public void update(float delta) {
        if (body != null) {
            if (getUserData().isOnGround()) {
                timeToDelete -= delta;
                if (timeToDelete <= 0) {
                    userData.setNeedDelete(true);
                }
            }
        }
    }

    @Override
    public PoisonBlastUserData getUserData() {
        return (PoisonBlastUserData) userData;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (body != null) {
            stateTime += Gdx.graphics.getDeltaTime();
            if (!getUserData().isOnGround()) {
                batch.draw((TextureRegion) walkLeftAnimation.getKeyFrame(stateTime, true), getX(), getY(), getWidth(), getHeight());
            } else {
                batch.draw((TextureRegion) stayLeftAnimation.getKeyFrame(stateTime, true), getX(), getY(), getWidth(), getHeight());
            }
        }
    }

    public void setupRandomDirection() {
        int i = MathUtils.random(1, 2);
        if (body != null) {
            if (i == 1) {
                getUserData().setDirection(Direction.LEFT);
            } else {
                getUserData().setDirection(Direction.RIGHT);
            }
        }
    }

    @Override
    public void setNeedDelete(boolean b) {
        getUserData().setNeedDelete(b);
    }
}
