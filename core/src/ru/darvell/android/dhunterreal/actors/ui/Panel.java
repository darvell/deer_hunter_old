package ru.darvell.android.dhunterreal.actors.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;

public class Panel extends Group{

    private TextureRegion panelTexture;

    private Label totalScoreLabel;
    private Label bestScoreLabel;
    private Skin skin;

    private String totalCaption = "Total score:  ";
    private String bestCaption = "Best score:   ";

    private Button newGameBtn;
    private Button resetGameBtn;

    public Panel(float centerX, float centerY, MyButtonListener listener){
        setX(0 - Constants.PANEL_WIDTH / 2);
        setY(centerY);
        setWidth(Constants.PANEL_WIDTH);
        setHeight(Constants.PANEL_HEIGHT);
        panelTexture = Assets.getTexture(Assets.PANEL);

        skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        initLabels();
        initButtons(listener);
        hide();
    }

    @Override
    public void draw(Batch batch, float a) {
        batch.draw(panelTexture, getX(), getY(), getWidth(), getHeight());
        super.draw(batch, a);
    }

    public void hide(){
        setVisible(false);
    }

    public void show(int totalScore, int bestScore){
        totalScoreLabel.setText(totalCaption + totalScore);
        bestScoreLabel.setText(bestCaption + bestScore);
        setVisible(true);
    }

    private void initLabels(){
        totalScoreLabel = new Label(totalCaption, skin);
        addActor(totalScoreLabel);
        totalScoreLabel.setX(60);
        totalScoreLabel.setY(230);

        bestScoreLabel = new Label(bestCaption, skin);
        addActor(bestScoreLabel);
        bestScoreLabel.setX(totalScoreLabel.getX());
        bestScoreLabel.setY(180);
    }

    private void initButtons(MyButtonListener listener){
        newGameBtn = ButtonFabric.getButton(60, 20, ButtonTypes.PANEL_TYPE, ButtonActionTypes.PANEL_NEW_GAME, listener);
        addActor(newGameBtn);
        resetGameBtn = ButtonFabric.getButton(60 + Constants.BUTTON_PANEL_WIDTH + 40, 20, ButtonTypes.PANEL_TYPE, ButtonActionTypes.PANEL_RESET_GAME, listener);
        addActor(resetGameBtn);
    }



}
