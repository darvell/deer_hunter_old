package ru.darvell.android.dhunterreal.actors.ui;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import ru.darvell.android.dhunterreal.utils.Constants;

public class Button extends Actor {

    private TextureRegion normallTexture;
    private TextureRegion pressedTexture;

    private boolean pressed = false;

    private ButtonActionTypes actionType;
    private ButtonTypes type;

    private MyButtonListener buttonListener;

    public Button(float x, float y, ButtonTypes type, ButtonActionTypes actionType, TextureRegion normallTexture, TextureRegion pressedTexture) {
        setX(x);
        setY(y);
        if (type == ButtonTypes.CONTROL_TYPE) {
            setWidth(Constants.BUTTON_STNDRT_SIZE);
            setHeight(Constants.BUTTON_STNDRT_SIZE);

        } else if (type == ButtonTypes.TITLE_TYPE) {
            setWidth(Constants.BUTTON_TITLE_WIDTH);
            setHeight(Constants.BUTTON_TITLE_HEIGHT);

        } else if ((type == ButtonTypes.PANEL_TYPE) || (type == ButtonTypes.SERVICE_TYPE)) {
            setWidth(Constants.BUTTON_PANEL_WIDTH);
            setHeight(Constants.BUTTON_PANEL_HEIGHT);

        } else if ((type == ButtonTypes.CAT_SHOP) ) {
            setWidth(Constants.BUTTON_CAT_SHOP_WIDTH);
            setHeight(Constants.BUTTON_CAT_SHOP_HEIGHT);

        }
        this.normallTexture = normallTexture;
        this.pressedTexture = pressedTexture;
        this.type = type;
        this.actionType = actionType;
    }

    public void setListener(MyButtonListener listener) {
        this.buttonListener = listener;
        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                buttonListener.onTouchDownControlButton(type, actionType);
                setPressed(true);
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                buttonListener.onTouchUpControlButton(type, actionType);
                setPressed(false);
            }
        });

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        TextureRegion textureToDraw = isPressed() ? pressedTexture : normallTexture;
        batch.draw(textureToDraw, getX(), getY(), getWidth(), getHeight());
    }

    public boolean isPressed() {
        return pressed;
    }

    public void setPressed(boolean pressed) {
        this.pressed = pressed;
    }

    public ButtonActionTypes getActionType() {
        return actionType;
    }


}
