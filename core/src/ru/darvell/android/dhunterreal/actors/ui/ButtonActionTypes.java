package ru.darvell.android.dhunterreal.actors.ui;

public enum ButtonActionTypes {
    // On Game Screen
    LEFT, RIGHT, SHOOT, JUMP,
    //Plank
    NEW_GAME, CREDITS, CAT, FEED,
    //On panel
    PANEL_NEW_GAME, PANEL_RESET_GAME,
    MUSIC_ON, MUSIC_OFF, BACK,
    BUY_BEER, BUY_SALAD, BUY_MEAT

}
