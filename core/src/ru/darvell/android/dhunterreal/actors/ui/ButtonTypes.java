package ru.darvell.android.dhunterreal.actors.ui;

public enum ButtonTypes {
    CONTROL_TYPE,
    TITLE_TYPE,
    PANEL_TYPE,
    SERVICE_TYPE,
    CAT_SHOP
}
