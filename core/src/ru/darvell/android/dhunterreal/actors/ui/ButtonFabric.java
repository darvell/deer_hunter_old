package ru.darvell.android.dhunterreal.actors.ui;

import ru.darvell.android.dhunterreal.utils.Assets;

public class ButtonFabric {

    public static Button getButton(float x, float y, ButtonTypes type, ButtonActionTypes actionType, MyButtonListener listener) {
        Button result = null;
        switch (actionType) {
            case LEFT:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_LEFT_NRML), Assets.getTexture(Assets.BTN_LEFT_PRSSD));
                break;
            case RIGHT:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_RIGHT_NRML), Assets.getTexture(Assets.BTN_RIGHT_PRSSD));
                break;
            case SHOOT:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_SHOOT_NRML), Assets.getTexture(Assets.BTN_SHOOT_PRSSD));
                break;
            case JUMP:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_JUMP_NRML), Assets.getTexture(Assets.BTN_JUMP_PRSSD));
                break;
            case NEW_GAME:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_PLANK_PLAY), Assets.getTexture(Assets.BTN_PLANK_PLAY));
                break;
            case CREDITS:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_PLANK_CREDITS), Assets.getTexture(Assets.BTN_PLANK_CREDITS));
                break;
            case PANEL_NEW_GAME:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_PANEL_NEW_GAME_NRML), Assets.getTexture(Assets.BTN_PANEL_NEW_GAME_NRML));
                break;
            case PANEL_RESET_GAME:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_PANEL_RESET_GAME_NRML), Assets.getTexture(Assets.BTN_PANEL_RESET_GAME_NRML));
                break;
            case MUSIC_OFF:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_MUSIC_OFF_NRML), Assets.getTexture(Assets.BTN_MUSIC_OFF_NRML));
                break;
            case MUSIC_ON:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_MUSIC_ON_NRML), Assets.getTexture(Assets.BTN_MUSIC_ON_NRML));
                break;
            case BACK:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_BACK_NRML), Assets.getTexture(Assets.BTN_BACK_NRML));
                break;
            case CAT:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_PLANK_CAT), Assets.getTexture(Assets.BTN_PLANK_CAT));
                break;
            case FEED:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_PLANK_FEED), Assets.getTexture(Assets.BTN_PLANK_FEED));
                break;
            case BUY_BEER:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_CAT_SHOP_BEER), Assets.getTexture(Assets.BTN_CAT_SHOP_BEER));
                break;
            case BUY_SALAD:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_CAT_SHOP_SALAD), Assets.getTexture(Assets.BTN_CAT_SHOP_SALAD));
                break;
            case BUY_MEAT:
                result = new Button(x, y, type, actionType, Assets.getTexture(Assets.BTN_CAT_SHOP_MEAT), Assets.getTexture(Assets.BTN_CAT_SHOP_MEAT));
                break;
        }
        if (result != null) {
            result.setListener(listener);
        }
        return result;
    }

}
