package ru.darvell.android.dhunterreal.actors.ui;

public interface MyButtonListener {

    void onTouchDownControlButton(ButtonTypes buttonTypes, ButtonActionTypes type);
    void onTouchUpControlButton(ButtonTypes buttonTypes, ButtonActionTypes type);

}
