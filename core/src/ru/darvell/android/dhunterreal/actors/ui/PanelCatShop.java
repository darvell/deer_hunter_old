package ru.darvell.android.dhunterreal.actors.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import ru.darvell.android.dhunterreal.MySkuDetails;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;

import java.util.List;

public class PanelCatShop extends Group {

    private TextureRegion panelTexture;
    private Skin skin;
    private MyButtonListener listener;

    private Button btnBeer;
    private Button btnSalad;
    private Button btnMeet;

    private Label labelBeerPrice;
    private Label labelSaladPrice;
    private Label labelMeatPrice;

    public PanelCatShop(float x, float y, MyButtonListener listener) {
        setX(x);
        setY(y);
        setWidth(Constants.CAT_PANEL_WIDTH);
        setHeight(Constants.CAT_PANEL_HEIGHT);
        panelTexture = Assets.getTexture(Assets.PANEL);

        this.listener = listener;
        skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        initButtons();
        initLabels();
    }

    @Override
    public void draw(Batch batch, float a) {
        batch.draw(panelTexture, getX(), getY(), getWidth(), getHeight());
        super.draw(batch, a);
    }

    private void initLabels(){
        labelBeerPrice = new Label("", skin);
        labelBeerPrice.setX(35 + btnBeer.getWidth() + 20);
        labelBeerPrice.setY(btnBeer.getY() + 40);
        addActor(labelBeerPrice);

        labelSaladPrice = new Label("", skin);
        labelSaladPrice.setX(35 + btnSalad.getWidth() + 20);
        labelSaladPrice.setY(btnSalad.getY() + 40);
        addActor(labelSaladPrice);

        labelMeatPrice = new Label("", skin);
        labelMeatPrice.setX(35 + btnMeet.getWidth() + 20);
        labelMeatPrice.setY(btnMeet.getY() + 40);
        addActor(labelMeatPrice);
    }

    private void initButtons() {
        float space = (getHeight() - (Constants.BUTTON_CAT_SHOP_HEIGHT * 3)) / 5;
        btnBeer = ButtonFabric.getButton(35, getHeight() - Constants.BUTTON_CAT_SHOP_HEIGHT - space - 10, ButtonTypes.CAT_SHOP, ButtonActionTypes.BUY_BEER, listener);
        addActor(btnBeer);
        btnSalad = ButtonFabric.getButton(35, btnBeer.getY() - Constants.BUTTON_CAT_SHOP_HEIGHT - space, ButtonTypes.CAT_SHOP, ButtonActionTypes.BUY_SALAD, listener);
        addActor(btnSalad);
        btnMeet = ButtonFabric.getButton(35, btnSalad.getY() - Constants.BUTTON_CAT_SHOP_HEIGHT - space, ButtonTypes.CAT_SHOP, ButtonActionTypes.BUY_MEAT, listener);
        addActor(btnMeet);
    }

    public void refresh(List<MySkuDetails> skuDetailsList){
        for (MySkuDetails skuDetails: skuDetailsList){
            if (skuDetails.getSkuName().equals(Constants.SKU_BEER)){
                labelBeerPrice.setText(skuDetails.getPrice());
            } else if (skuDetails.getSkuName().equals(Constants.SKU_SALAD)){
                labelSaladPrice.setText(skuDetails.getPrice());
            } else if (skuDetails.getSkuName().equals(Constants.SKU_MEAT)){
                labelMeatPrice.setText(skuDetails.getPrice());
            }

        }
    }
}
