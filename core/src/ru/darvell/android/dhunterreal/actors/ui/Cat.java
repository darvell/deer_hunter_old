package ru.darvell.android.dhunterreal.actors.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;

public class Cat extends Actor {

    private Animation sadCatAnimation;
    private float blinkPause;

    private boolean blinking = true;
    private float stateTime;

    public Cat(float x, float y) {
        setX(x);
        setY(y);
        setWidth(Constants.SAD_CAT_WIDTH);
        setHeight(Constants.SAD_CAT_HEIGHT);
        sadCatAnimation = Assets.getRepeatAnimation(Assets.SAD_CAT, 0.15f,0, 15);
    }

    @Override
    public void draw(Batch batch, float a) {
        super.draw(batch, a);
        stateTime += Gdx.graphics.getDeltaTime();
        TextureRegion textureToDraw = null;
        if (blinking) {
            textureToDraw = (TextureRegion) sadCatAnimation.getKeyFrame(stateTime, true);
        }
        batch.draw(textureToDraw, getX(), getY(), getWidth(), getHeight());
    }


    private TextureRegion getDefaultTextureRegion() {
        return (TextureRegion) sadCatAnimation.getKeyFrames()[0];
    }


}
