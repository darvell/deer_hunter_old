package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.android.dhunterreal.actors.interfaces.Enemy;
import ru.darvell.android.dhunterreal.box2d.DeerUserData;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.SoundPlayer;


public class Deer extends Person  implements Enemy {


    private Vector2 velocity = new Vector2();
    private SoundPlayer soundPlayer;
    private long currSoundId;

    public Deer(Body body, SoundPlayer soundPlayer) {
        super(body);
        walkLeftAnimation = Assets.getAnimation(Assets.DEER_WALK_RIGHT, 0.1f);
        walkRightAnimation = Assets.getAnimation(Assets.DEER_WALK_LEFT, 0.1f);
        stateTime = 0f;
        this.soundPlayer = soundPlayer;
    }

    public Direction getDirection() {
        return getUserData().getDirection();
    }

    public void setDirection(Direction direction) {
        getUserData().setDirection(direction);
    }

    @Override
    public DeerUserData getUserData() {
        return (DeerUserData) userData;
    }

    public void initMoving() {
        setMoving(true);
        if (getDirection().equals(Direction.RIGHT)) {
            velocity.x = getUserData().getSpeed();
        } else {
            velocity.x = -getUserData().getSpeed();
        }

        currSoundId = soundPlayer.playRunningDeer();
    }

    public void update(float delta) {
        if (getUserData().isNeedChangeDirection()) {
            resetMoveDirection();
        }
        if (getUserData().isNeedJump()) {
            jump();
            getUserData().setNeedJump(false);
        }
        Vector2 tmpVel = body.getLinearVelocity();
        velocity.y = tmpVel.y;
        body.setLinearVelocity(velocity);
    }

    public void stopMove() {
        soundPlayer.stopRunningDeer(currSoundId);
        setMoving(false);
        velocity.x = 0;
        velocity.y = 0;
        stateTime = 0;

    }

    public void resetMoveDirection() {
        stopMove();
        getUserData().setNeedChangeDirection(false);
        if (getDirection().equals(Direction.RIGHT)) {
            setDirection(Direction.LEFT);
        } else {
            setDirection(Direction.RIGHT);
        }
        initMoving();
    }

    public void jump() {
        if (!isJumping()) {
            body.applyLinearImpulse(getUserData().getJumpingLinearImpulse(), body.getWorldCenter(), true);
            setJumping(true);
            resetFriction();
        }
    }

    public void landed() {
        setJumping(true);
        setFriction();
    }

    private void setFriction() {
        getUserData().getFixture().setFriction(getUserData().getFriction());
    }

    private void resetFriction() {
        getUserData().getFixture().setFriction(0);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if (body != null) {
            stateTime += Gdx.graphics.getDeltaTime();
            if (isJumping()) {
                if (getUserData().getDirection().equals(Direction.LEFT)) {
                    batch.draw((TextureRegion) walkRightAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
                } else {
                    batch.draw((TextureRegion) walkLeftAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
                }

            } else if (isMoving()) {
                if (getUserData().getDirection().equals(Direction.LEFT)) {
                    batch.draw((TextureRegion) walkRightAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
                } else {
                    batch.draw((TextureRegion) walkLeftAnimation.getKeyFrame(stateTime, true), getXForDraw(), getY(), getWidth(), getHeight());
                }
            }
        }
    }

    private float getXForDraw() {
        return getUserData().getDirection().equals(Direction.RIGHT) ?
                (getX() - transformToScreen(getUserData().getWidth() / 2)) + 7
                : (getX() - transformToScreen(getUserData().getWidth() / 2));
    }

    @Override
    public void setNeedDelete(boolean b) {
        getUserData().setNeedDelete(true);
    }
}
