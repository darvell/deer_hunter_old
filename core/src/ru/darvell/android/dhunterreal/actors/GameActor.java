package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import ru.darvell.android.dhunterreal.box2d.UserData;
import ru.darvell.android.dhunterreal.utils.Constants;

public abstract class GameActor extends Actor {

    protected Body body;
    protected UserData userData;
    protected float sizeWidth = 64f;
    protected float sizeHeight = 64f;

    private boolean jumping;
    private boolean moving;

    public GameActor(Body body) {
        this.body = body;
        this.userData = (UserData) body.getUserData();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if ((body != null) && (body.getUserData() != null)){
            updateRectangle();
        } else {
            remove();
        }
    }

    public abstract UserData getUserData() throws NullPointerException;


    protected void updateRectangle() {
        setX(transformToScreen(body.getPosition().x - userData.getWidth() / 2));
        setY(transformToScreen(body.getPosition().y - userData.getWidth() / 2));
        setWidth(sizeWidth * 2);
        setHeight(sizeHeight * 2);
    }

    protected float transformToScreen(float n) {
        return Constants.WORLD_TO_SCREEN * n;
    }

    public Body getBody(){
        return body;
    }

    public void resetBody(){
        body = null;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public boolean isMoving() {
        return moving;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }
}
