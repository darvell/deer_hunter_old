package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.StateMachine;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.android.dhunterreal.actors.ai.ThinLarryState;
import ru.darvell.android.dhunterreal.actors.interfaces.Boss;
import ru.darvell.android.dhunterreal.box2d.ThinLarryUserData;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;
import ru.darvell.android.dhunterreal.utils.GdxLog;
import ru.darvell.android.dhunterreal.utils.SoundPlayer;

import static ru.darvell.android.dhunterreal.actors.ai.ThinLarryState.*;

public class ThinLarry extends Person implements Boss {

    public static final String TAG = "ThinLarry";

    public StateMachine<ThinLarry, ThinLarryState> stateMachine;
    private Vector2 velocity = new Vector2();
    private boolean needPoisonBlast = false;
    private boolean needCenterPoisonBlast = false;
    private boolean centerReached = false;

    private float waitingTime = 0;
    private float chargingTime = 0;
    private float openMothTime = 0;

    private boolean killedByPlayer = false;

    private Animation chargeRightAnimation;
    private Animation chargeLeftAnimation;
    private Animation blastingLeftAnimation;
    private Animation blastingRightAnimation;
    private Animation blastingCenterLeftAnimation;
    private Animation blastingCenterRightAnimation;
    private Animation openRightAnimation;
    private Animation openLeftAnimation;

    private int lifes = 0;

    private long currSoundId;
    private SoundPlayer soundPlayer;

    public ThinLarry(Body body, SoundPlayer soundPlayer) {
        super(body);
        lifes = Constants.LARRY_LIFE_COUNT;
        stateMachine = new DefaultStateMachine<ThinLarry, ThinLarryState>(this, WALK);
        sizeWidth = 86f;
        sizeHeight = 86f;
        walkRightAnimation = Assets.getAnimation(Assets.THIN_LARRY_WALK_LEFT, 0.15f);
        walkLeftAnimation = Assets.getAnimation(Assets.THIN_LARRY_WALK_RIGHT, 0.15f);
        stayRightAnimation = Assets.getAnimation(Assets.THIN_LARRY_STAY_RIGHT, 0.15f);
        stayLeftAnimation = Assets.getAnimation(Assets.THIN_LARRY_STAY_LEFT, 0.15f);
        chargeRightAnimation = Assets.getAnimation(Assets.THIN_LARRY_CHARGE_RIGHT, 0.1f);
        chargeLeftAnimation = Assets.getAnimation(Assets.THIN_LARRY_CHARGE_LEFT, 0.1f);
        blastingLeftAnimation = Assets.getAnimation(Assets.THIN_LARRY_BLAST_LEFT, 0.1f);
        blastingRightAnimation = Assets.getAnimation(Assets.THIN_LARRY_BLAST_RIGHT, 0.1f);
        openLeftAnimation = Assets.getAnimation(Assets.THIN_LARRY_OPEN_LEFT, 0.2f);
        openRightAnimation = Assets.getAnimation(Assets.THIN_LARRY_OPEN_RIGHT, 0.2f);
        blastingCenterLeftAnimation = Assets.getAnimation(Assets.THIN_LARRY_BLAST_CENTER_LEFT, 0.1f);
        blastingCenterRightAnimation = Assets.getAnimation(Assets.THIN_LARRY_BLAST_CENTER_RIGHT, 0.1f);
        stateTime = 0f;
        this.soundPlayer = soundPlayer;
    }

    @Override
    public ThinLarryUserData getUserData() throws NullPointerException {
        return (ThinLarryUserData) this.userData;
    }

    @Override
    public void draw(Batch batch, float a) {
        super.draw(batch, a);
        stateTime += Gdx.graphics.getDeltaTime();
        try {
            if (stateMachine.getCurrentState().equals(WALK)) {
                if (getUserData().getDirection().equals(Direction.LEFT)) {
                    batch.draw((TextureRegion) walkRightAnimation.getKeyFrame(stateTime, true), getXForDraw(), getYForDraw(), getWidth(), getHeight());
                } else {
                    batch.draw((TextureRegion) walkLeftAnimation.getKeyFrame(stateTime, true), getXForDraw(), getYForDraw(), getWidth(), getHeight());
                }
            } else if (stateMachine.getCurrentState().equals(CHARGE)) {
                if (getUserData().getDirection().equals(Direction.LEFT)) {
                    batch.draw((TextureRegion) chargeLeftAnimation.getKeyFrame(stateTime, true), getXForDraw(), getYForDraw(), getWidth(), getHeight());
                } else {
                    batch.draw((TextureRegion) chargeRightAnimation.getKeyFrame(stateTime, true), getXForDraw(), getYForDraw(), getWidth(), getHeight());
                }
            } else if (stateMachine.getCurrentState().equals(BLASTING)) {
                if (getUserData().getDirection().equals(Direction.LEFT)) {
                    batch.draw((TextureRegion) blastingLeftAnimation.getKeyFrame(stateTime, true), getXForDraw(), getYForDraw(), getWidth(), getHeight());
                } else {
                    batch.draw((TextureRegion) blastingRightAnimation.getKeyFrame(stateTime, true), getXForDraw(), getYForDraw(), getWidth(), getHeight());
                }
            } else if (stateMachine.getCurrentState().equals(OPEN_MOTH)) {
                if (getUserData().getDirection().equals(Direction.LEFT)) {
                    batch.draw((TextureRegion) openLeftAnimation.getKeyFrame(stateTime, false), getXForDraw(), getYForDraw(), getWidth(), getHeight());
                } else {
                    batch.draw((TextureRegion) openRightAnimation.getKeyFrame(stateTime, false), getXForDraw(), getYForDraw(), getWidth(), getHeight());
                }
            }  else if (stateMachine.getCurrentState().equals(BLASTING_CENTER)) {
                if (getUserData().getDirection().equals(Direction.LEFT)) {
                    batch.draw((TextureRegion) blastingCenterLeftAnimation.getKeyFrame(stateTime, true), getXForDraw(), getYForDraw(), getWidth(), getHeight());
                } else {
                    batch.draw((TextureRegion) blastingCenterRightAnimation.getKeyFrame(stateTime, true), getXForDraw(), getYForDraw(), getWidth(), getHeight());
                }
            }
        } catch (NullPointerException e) {
            GdxLog.d(TAG, "to late");
        }

//        Body body  = getUserData().getFixture().getBody();
//
//        batch.draw(Assets.getTexture(Assets.TEST_SQUARE), transformToScreen(body.getPosition().x - getUserData().getWidth()/2),
//                transformToScreen(body.getPosition().y - getUserData().getWidth()/2),
//                transformToScreen(getUserData().getWidth()),
//                transformToScreen(getUserData().getHeight())
//        );


//        Body bodyHead = getUserData().getHeadFixture().getBody();
//
//
//        if (getUserData().getDirection().equals(Direction.LEFT)) {
//            batch.draw(Assets.getTexture(Assets.TEST_SQUARE),
//                    transformToScreen(body.getPosition().x - getUserData().getWidth()/2),
//                    transformToScreen(bodyHead.getPosition().y + getUserData().getHeight() / 2),
//                    transformToScreen(0.5f),
//                    transformToScreen(0.5f)
//            );
//        } else {
//
//            batch.draw(Assets.getTexture(Assets.TEST_SQUARE),
//                    transformToScreen((body.getPosition().x + getUserData().getWidth()/2)-0.5f),
//                    transformToScreen(bodyHead.getPosition().y + getUserData().getHeight() / 2),
//                    transformToScreen(0.5f),
//                    transformToScreen(0.5f)
//            );
//        }
    }


    private float getXForDraw() {
        return getUserData().getDirection().equals(Direction.RIGHT) ?
                (getX() - transformToScreen(getUserData().getWidth() / 2) + 12)
                : (getX() - transformToScreen(getUserData().getWidth() / 2));
    }

    private float getYForDraw() {
        return getY() + 10;
    }


    public Vector2 getPosition() {
        if (body != null) {
            return body.getPosition();
        } else {
            return null;
        }
    }

    @Override
    public void setNeedDelete(boolean b) {
        getUserData().setNeedDelete(b);
    }

    public void update(float delta) {
        if (body != null) {
            if (lifes > 0) {
                stateMachine.update();
                switch (stateMachine.getCurrentState()) {
                    case CHARGE:
                        chargingTime -= delta;
                        break;

                    case BLASTING:
                        waitingTime -= delta;
                        break;

                    case BLASTING_CENTER:
                        waitingTime -= delta;
                        break;

                    case WALK:
                        Vector2 tmpVel = body.getLinearVelocity();
                        velocity.y = tmpVel.y;
                        body.setLinearVelocity(velocity);
                        break;

                    case OPEN_MOTH:
                        openMothTime -= delta;
                        break;
                }
            }
        }
    }

    public void resetMoveDirection() {
        getUserData().setNeedChangeDirection(false);
        if (getUserData().getDirection().equals(Direction.RIGHT)) {
            getUserData().setDirection(Direction.LEFT);
        } else {
            getUserData().setDirection(Direction.RIGHT);
        }
    }

    public void stopMove() {
        setMoving(false);
        velocity.x = 0;
        velocity.y = 0;
        stateTime = 0;
        soundPlayer.stopRunningDeer(currSoundId);
    }

    public void initMoving() {
        setMoving(true);
        if (getUserData().getDirection().equals(Direction.RIGHT)) {
            velocity.x = getUserData().getSpeed();
        } else {
            velocity.x = -getUserData().getSpeed();
        }
        currSoundId = soundPlayer.playRunningDeer();
    }

    public Direction getDirection() {
        return getUserData().getDirection();
    }

    public boolean isNeedPoisonBlast() {
        return needPoisonBlast;
    }

    public void setNeedPoisonBlast(boolean needPoisonBlast) {
        this.needPoisonBlast = needPoisonBlast;
    }

    public boolean isNeedCenterPoisonBlast() {
        return needCenterPoisonBlast;
    }

    public void setNeedCenterPoisonBlast(boolean needCenterPoisonBlast) {
        this.needCenterPoisonBlast = needCenterPoisonBlast;
    }

    public boolean isNeedChangeDirection() {
        return getUserData().isNeedChangeDirection();
    }

    public void preparePause() {
        waitingTime = Constants.LARRY_WAIT_TIME;
    }

    public float getPauseTime() {
        return waitingTime;
    }

    public void resetPauseTime() {
        waitingTime = 0;
    }

    public void prepareCharge() {
        chargingTime = Constants.LARRY_CHARGING_TIME;
    }

    public float getChargingTime() {
        return chargingTime;
    }

    public void resetChargingTime() {
        chargingTime = 0;
    }

    public void prepareOpenMoth() {
        openMothTime = Constants.LARRY_OPEN_MOTH_TIME;
    }

    public float getOpenMothTime() {
        return openMothTime;
    }

    public void resetOpenMothTime() {
        openMothTime = 0;
    }

    public void hit() {
        lifes -= 1;
    }

    public int getLifes() {
        return lifes;
    }

    public boolean isKilledByPlayer() {
        return killedByPlayer;
    }

    public void setKilledByPlayer(boolean killedByPlayer) {
        this.killedByPlayer = killedByPlayer;
    }

    public boolean isCenterReached() {
        return centerReached;
    }

    public void setCenterReached(boolean centerReached) {
        this.centerReached = centerReached;
    }

    public int getWinDiceCount(){
        return (getLifes() < Constants.LARRY_LIFE_COUNT/2)?7:8;
    }
}
