package ru.darvell.android.dhunterreal.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.android.dhunterreal.actors.interfaces.AnyBoodies;
import ru.darvell.android.dhunterreal.box2d.BulletUserData;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.utils.Assets;

public class Bullet extends Person implements AnyBoodies {

    public Bullet(Body body) {
        super(body);
        walkLeftAnimation = Assets.getAnimation(Assets.BULLET, 0.01f);
    }

    @Override
    public BulletUserData getUserData() {
        return (BulletUserData) userData;
    }

    public void shoot(){
        Vector2 vector2 = new Vector2();
        vector2.y = 0;
        if (getUserData().getDirection().equals(Direction.LEFT)){
            vector2.x = -getUserData().getBulletSpeed();
        } else {
            vector2.x = getUserData().getBulletSpeed();
        }
        body.applyLinearImpulse(vector2, body.getWorldCenter(), true);
    }

    public boolean isNeedDelete(){
        return getUserData().isNeedDelete();
    }

    public Body getBody(){
        return body;
    }

    @Override
    protected void updateRectangle() {
        setX(transformToScreen(body.getPosition().x - userData.getWidth()));
        setY(transformToScreen(body.getPosition().y - userData.getWidth() / 2));
        setWidth(6 * 2);
        setHeight(6 * 2);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        stateTime += Gdx.graphics.getDeltaTime();
        batch.draw((TextureRegion) walkLeftAnimation.getKeyFrame(stateTime, false), getX(), getY(), getWidth(), getHeight());
    }

    @Override
    public void setNeedDelete(boolean b) {
        getUserData().setNeedDelete(b);
    }

    @Override
    public void update(float delta) {

    }
}
