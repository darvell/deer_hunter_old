package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import ru.darvell.android.dhunterreal.actors.Deer;
import ru.darvell.android.dhunterreal.actors.Ground;
import ru.darvell.android.dhunterreal.actors.Rabbit;
import ru.darvell.android.dhunterreal.actors.ThinLarry;
import ru.darvell.android.dhunterreal.actors.interfaces.Boss;
import ru.darvell.android.dhunterreal.actors.interfaces.Enemy;
import ru.darvell.android.dhunterreal.screens.ParentScreen;

import java.util.Iterator;

public class MobsUtils extends BodyActorsUtils{

    public static final int BOSS_LARRY = 1;

    private Array<Enemy> enemies = new Array();
    private GamePlan gamePlan;

    private Boss currBoss;

    private ParentScreen parentScreen;


    public MobsUtils(ParentScreen parentScreen, GamePlan gamePlan, World world) {
        super(world);
        this.gamePlan = gamePlan;
        this.parentScreen = parentScreen;
    }

    public void updateWithoutBosses(float delta){
        Iterator<Enemy> iterator = enemies.iterator();
        while (iterator.hasNext()) {
            Enemy enemy = iterator.next();
            if (!(enemy instanceof Boss)){
                if (enemy instanceof Deer){
                    Deer deer = (Deer) enemy;
                    if (deer.getUserData().isNeedDelete()) {
//                        deer.stopMove();
//                        iterator.remove();
                    } else {
                        deer.update(delta);
                    }
                } else {
                    enemy.update(delta);
                }
            }
        }
    }

    public Boss getCurrBoss() {
        return currBoss;
    }

    public int getEnemiesCount(){
        return enemies.size;
    }

    public int getMobsCount(Class<?> t){
        int count = 0;
        for (Enemy enemy: enemies){
            if (enemy.getClass().equals(t)){
                count++;
            }
        }
        return count;
    }

    public void createDeer(Stage stage, Ground ground) {
//        if (enemies.size < gamePlan.getMaxDeersOnLevel()) {
            int i = MathUtils.random(1, 2);

            Deer deer = new Deer(WorldUtils.createEnemy(world, ground.getPosition(), i == 1), parentScreen.getSoundPlayer());
            enemies.add(deer);
            stage.addActor(deer);
            deer.initMoving();
//        }
    }

    public void createRabbit(Stage stage, Ground ground) {
        int i = MathUtils.random(1, 2);
        Rabbit rabbit = new Rabbit(WorldUtils.createRabbit(world, ground.getPosition(), i == 1), parentScreen.getSoundPlayer());
        stage.addActor(rabbit);
        enemies.add(rabbit);
    }


    public void createBoss(int bossType, Stage stage, Ground ground, boolean enemyCornerRight){
        Boss boss;
        switch (bossType){
            case BOSS_LARRY: boss = createThinLarry(stage, ground, enemyCornerRight);
                    break;
            default: boss = createThinLarry(stage, ground, enemyCornerRight);
                break;
        }
        enemies.add(boss);
    }

    public ThinLarry createThinLarry(Stage stage, Ground ground, boolean enemyCornerRight) {
        ThinLarry thinLarry = new ThinLarry(WorldUtils.createThinLarry(world, ground.getPosition(), enemyCornerRight), parentScreen.getSoundPlayer());
        stage.addActor(thinLarry);
        thinLarry.initMoving();
        currBoss = thinLarry;
        enemies.add(thinLarry);
        return thinLarry;

    }

    public void markAllEnemysBodiesToDelete() {
        for (Enemy enemy : enemies) {
            enemy.setNeedDelete(true);
        }
    }

    public void deleteAllMarkedBodies() {
        Iterator<Enemy> iterator = enemies.iterator();
        while (iterator.hasNext()) {
            Enemy enemy = iterator.next();
            if (enemy instanceof Deer) {
                Deer deer = (Deer) enemy;
                if (checkAndDestroyBody(deer.getBody())) {
                    deer.resetBody();
                    deer.stopMove();
                    enemies.removeValue(enemy, true);
                    GdxLog.d("kill_deer", "kill deer");
                }
            }

            if ((enemy instanceof ThinLarry) && (checkAndDestroyBody(((ThinLarry) enemy).getBody()))) {
                ThinLarry thinLarry = (ThinLarry) enemy;
                if (thinLarry.isKilledByPlayer()) {
                    gamePlan.killLarry();
                }
                gamePlan.setBossState(false);
                thinLarry.resetBody();
                thinLarry.stopMove();
                thinLarry = null;
                enemies.removeValue(enemy, true);
            }

            if (enemy instanceof Rabbit){
                Rabbit rabbit = (Rabbit) enemy;
                if (checkAndDestroyBody(rabbit.getBody())){
                    rabbit.resetBody();
                    enemies.removeValue(rabbit, true);
                }
            }
        }
    }





//    private void resetAllDeadEnemies() {
//        for (DeadBody deadBody : deadBodies) {
//            deadBody.remove();
//        }
//        deadBodies.clear();
//    }
}
