package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MyRepeatAnimation extends Animation {

    public MyRepeatAnimation(float frameDuration, Array keyFrames, int repeatIndex, int repeatCount) {
        super(frameDuration, keyFrames);

        LinkedList<Object> repeatFrames = new LinkedList<Object>();
        for (int i = 0; i < repeatCount; i++){
            repeatFrames.addLast(keyFrames.get(repeatIndex));
        }

        List<Object> tmpList = new LinkedList<Object>(Arrays.asList(keyFrames.toArray()));

        tmpList.addAll(repeatIndex, repeatFrames);

        Array<Object> resultArray = new Array<Object>(tmpList.toArray());


        setKeyFrames(resultArray.items);
    }


}
