package ru.darvell.android.dhunterreal.utils;

import ru.darvell.android.dhunterreal.ActionResolver;

public class TestGamePlan extends GamePlan {

    public TestGamePlan(ActionResolver actionResolver) {
        super(actionResolver);
        maxEnemiesOnLevel = 0;
    }

    @Override
    protected void updateGameProcessParameters() {
        if ((getKilledDeerCount() > 1) && (getKilledDeerCount() < 3)){
            setBossState(true);
            setNeedBoss(true);
        }
    }
}
