package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.I18NBundle;

public class Assets {

    public static final String ATLAS = "data/game_atlas/atlas.atlas";
    public static final String STRINGS = "data/i18n/strings";

    public static final String FRAY_STAY_RIGHT = "fray_stay_right";
    public static final String FRAY_WALK_RIGHT = "fray_walk_right";

    public static final String FRAY_STAY_LEFT = "fray_stay_left";
    public static final String FRAY_WALK_LEFT = "fray_walk_left";

    public static final String FRAY_JUMP_RIGHT = "fray_jump_right";
    public static final String FRAY_JUMP_LEFT = "fray_jump_left";

    public static final String FRAY_BAD_JUMP_RIGHT = "fray_bad_jump_right";
    public static final String FRAY_BAD_JUMP_LEFT = "fray_bad_jump_left";

    public static final String FRAY_SHUT_RIGHT = "fray_shut_right";
    public static final String FRAY_SHUT_LEFT = "fray_shut_left";

    public static final String DEER_WALK_LEFT = "deer_walk_left";
    public static final String DEER_WALK_RIGHT = "deer_walk_right";
    public static final String DEER_DEAD_RIGHT = "deer_dead_right";
    public static final String DEER_DEAD_LEFT = "deer_dead_left";

    public static final String THIN_LARRY_WALK_LEFT = "thin_larry_walk_left";
    public static final String THIN_LARRY_WALK_RIGHT = "thin_larry_walk_right";
    public static final String THIN_LARRY_STAY_LEFT = "thin_larry_stay_left";
    public static final String THIN_LARRY_STAY_RIGHT = "thin_larry_stay_right";
    public static final String THIN_LARRY_CHARGE_RIGHT = "thin_larry_charge_right";
    public static final String THIN_LARRY_CHARGE_LEFT = "thin_larry_charge_left";
    public static final String THIN_LARRY_BLAST_LEFT = "thin_larry_blast_left";
    public static final String THIN_LARRY_BLAST_RIGHT = "thin_larry_blast_right";
    public static final String THIN_LARRY_OPEN_LEFT = "thin_larry_open_left";
    public static final String THIN_LARRY_OPEN_RIGHT = "thin_larry_open_right";
    public static final String THIN_LARRY_BLAST_CENTER_LEFT = "thin_larry_blast_center_left";
    public static final String THIN_LARRY_BLAST_CENTER_RIGHT = "thin_larry_blast_center_right";

    public static final String RABBIT_STAY_LEFT = "rabbit_stay_left";
    public static final String RABBIT_STAY_RIGHT = "rabbit_stay_right";
    public static final String RABBIT_JUMP_LEFT = "rabbit_jump_left";
    public static final String RABBIT_JUMP_RIGHT = "rabbit_jump_right";


    public static final String SAD_CAT = "cat_sad";
    public static final String CAT_SCREEN_BACKGROUND = "cat_screen_background";

    public static final String BULLET = "bullet";
    public static final String POISON_BLAST_NORMAL = "poison_blast_normal";
    public static final String POISON_BLAST_GROUND = "poison_blast_ground";

    public static final String BLUE_BACKGROUND = "blue_background";

    public static final String BTN_LEFT_NRML = "btn_left_nrml";
    public static final String BTN_LEFT_PRSSD = "btn_left_prssd";
    public static final String BTN_RIGHT_NRML = "btn_right_nrml";
    public static final String BTN_RIGHT_PRSSD = "btn_right_prssd";
    public static final String BTN_JUMP_NRML = "btn_jump_nrml";
    public static final String BTN_JUMP_PRSSD = "btn_jump_prssd";
    public static final String BTN_SHOOT_NRML = "btn_shoot_nrml";
    public static final String BTN_SHOOT_PRSSD = "btn_shoot_prssd";

    public static final String BTN_PLANK_PLAY = "plank_play";
    public static final String BTN_PLANK_CREDITS = "plank_credits";
    public static final String BTN_PLANK_CAT = "plank_cat";
    public static final String BTN_PLANK_FEED = "plank_feed";

    public static final String BTN_PANEL_NEW_GAME_NRML = "btn_panel_new_game_nrmll";
    public static final String BTN_PANEL_RESET_GAME_NRML = "btn_panel_reset_game_nrmll";

    public static final String BTN_MUSIC_ON_NRML = "btn_music_on_nrmll";
    public static final String BTN_MUSIC_OFF_NRML = "btn_music_off_nrmll";

    public static final String BTN_BACK_NRML = "btn_back_nrmll";

    public static final String BTN_CAT_SHOP_BEER = "btn_cat_shop_beer";
    public static final String BTN_CAT_SHOP_SALAD = "btn_cat_shop_salad";
    public static final String BTN_CAT_SHOP_MEAT = "btn_cat_shop_meat";


    public static final String LIFEBIRD_RED_STAY = "lifebird_red_stay";
    public static final String LIFEBIRD_RED_FLY = "lifebird_red_fly";

    public static final String MUSIC_BIRD_SING = "music_bird_green_sing";
    public static final String MUSIC_BIRD_STAY = "music_bird_green_stay";

    public static final String TITLE_BACKGROUND = "title_screen_background";
    public static final String GAME_BACKGROUND = "world";

    public static final String PANEL = "panel";

    public static final String TEST_SQUARE = "test_square";

    public static final String MUSIC_BACK = "data/music/music_back.mp3";
    public static final String MUSIC_TITLE = "data/music/eternal_terminal.mp3";
    public static final String MUSIC_BOSS_FIGHT = "data/music/boss_fight.mp3";

    public static final String SOUND_SHOT = "data/sound/shot.wav";
    public static final String SOUND_SHOT2 = "data/sound/shot2.wav";
    public static final String SOUND_JUMP = "data/sound/jump.wav";
    public static final String SOUND_LANDING = "data/sound/landing.wav";
    public static final String SOUND_RUNNING = "data/sound/running.wav";
    public static final String SOUND_DEER_HIT = "data/sound/deer_hit.wav";
    public static final String SOUND_BIRD_FALL = "data/sound/bird_fall.wav";
    public static final String SOUND_BIRD_PICKUP = "data/sound/bird.wav";
    public static final String SOUND_SPLASH = "data/sound/splash.wav";
    public static final String SOUND_VOMIT = "data/sound/vomit.wav";
    public static final String SOUND_LARRY_ROAR = "data/sound/larry_roar.wav";
    public static final String SOUND_LARRY_LAUGHT = "data/sound/larry_laught.wav";
    public static final String SOUND_RABBIT_JUMP = "data/sound/rabbit_jump.wav";

    private static AssetManager manager;

    public static void load(){
        manager = new AssetManager();
        manager.load(ATLAS, TextureAtlas.class);
        manager.load(STRINGS, I18NBundle.class);
        manager.load(MUSIC_BACK, Music.class);
        manager.load(MUSIC_TITLE, Music.class);
        manager.load(MUSIC_BOSS_FIGHT, Music.class);

        reloadSounds();

    }

    public static void reloadSounds(){
        manager.load(SOUND_SHOT, Sound.class);
        manager.load(SOUND_SHOT2, Sound.class);
        manager.load(SOUND_JUMP, Sound.class);
        manager.load(SOUND_LANDING, Sound.class);
        manager.load(SOUND_RUNNING, Sound.class);
        manager.load(SOUND_DEER_HIT, Sound.class);
        manager.load(SOUND_BIRD_FALL, Sound.class);
        manager.load(SOUND_BIRD_PICKUP, Sound.class);
        manager.load(SOUND_SPLASH, Sound.class);
        manager.load(SOUND_VOMIT, Sound.class);
        manager.load(SOUND_LARRY_ROAR, Sound.class);
        manager.load(SOUND_LARRY_LAUGHT, Sound.class);
        manager.load(SOUND_RABBIT_JUMP, Sound.class);
    }

    public static void finishLoading(){
        manager.finishLoading();
    }

    public static void dispose(){
        manager.dispose();
        manager.clear();
    }

    private static TextureAtlas getAtlas(){
        return manager.get(ATLAS, TextureAtlas.class);
    }

    public static TextureRegion getTexture(String name){
        return getAtlas().findRegion(name);
    }

    public static I18NBundle getBundle(String name){
        return manager.get(name, I18NBundle.class);
    }

    public static Animation getAnimation(String animationType, float rate){
        Array<TextureAtlas.AtlasRegion> atlasRegions = getAtlas().findRegions(animationType);
        Animation animation = new Animation(rate, atlasRegions);
        return animation;
    }

    public static Animation getRepeatAnimation(String animationType, float rate, int repeatIndex, int repeatCount){
        Array<TextureAtlas.AtlasRegion> atlasRegions = getAtlas().findRegions(animationType);
        Animation animation = new MyRepeatAnimation(rate, atlasRegions, repeatIndex, repeatCount);
        return animation;
    }

    public static Music getMusic(String name){
        return manager.get(name, Music.class);
    }

    public static Sound getSound(String name){
        return manager.get(name, Sound.class);
    }
}
