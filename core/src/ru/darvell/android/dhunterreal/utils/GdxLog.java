package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.Gdx;

public class GdxLog {

    public static boolean DEBUG;

    @SuppressWarnings("all")
    public static void print(String tag, String message) {
        if (DEBUG) {
            Gdx.app.log(tag, message);
        }
    }

    @SuppressWarnings("all")
    public static void d(String tag, String message) {
        if (DEBUG) {
            Gdx.app.log(tag, message);
        }
    }
}
