package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;

public class MusicPlayer {

    private Music musicBack;
    private Music musicTitle;
    private Music bossFight;
    private Music current;

    private boolean enabled;

    public MusicPlayer(){
        musicBack = Assets.getMusic(Assets.MUSIC_BACK);
        musicBack.setLooping(true);
        musicBack.setVolume(0.6f);

        musicTitle = Assets.getMusic(Assets.MUSIC_TITLE);
        musicTitle.setLooping(true);
        musicTitle.setVolume(0.6f);

        bossFight = Assets.getMusic(Assets.MUSIC_BOSS_FIGHT);
        bossFight.setLooping(true);
        bossFight.setVolume(0.6f);

        enabled = false;
    }

    public void play(){
        if (isEnabled()) {
            current.play();
        }
    }

    public void stop(){
        current.stop();
    }

    public void pause(){
        current.pause();
    }

    public void changeToTitle(){
        musicBack.stop();
        current = musicTitle;
    }

    public void changeToBack(){
        musicTitle.stop();
        current = musicBack;
    }

    public void changeToBoss(){
        musicTitle.stop();
        current = bossFight;
    }


    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        if (current != null) {
            if (enabled) {
                current.play();
            } else {
                current.stop();
            }
        }
        storePref();
    }

    public void storePref(){
        Preferences prefs = Gdx.app.getPreferences(Constants.PREF_NAME);
        prefs.putBoolean(Constants.PREF_MUSIC_ENABLED, enabled);
        prefs.flush();
    }
}
