package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.math.Vector2;

public class Constants {

    public static final int APP_WIDTH = 1280;
    public static final int APP_HEIGHT = 960;

    public static final Vector2 WORLD_GRAVITY = new Vector2(0, -10);
    public static final float WORLD_TO_SCREEN = 67.37f;

    public static final float GROUND_X = 0;
    public static final float GROUND_Y = 4.51f;
    public static final float GROUND_WIDTH = 24f;
    public static final float GROUND_HEIGHT = 1f;
    public static final float GROUND_DENSITY = 0f;

    public static final float TIMEOUT_TIME = 1.5f;

    public static final float WORLD_SENSOR_WIDTH = 1;

    public static final float RUNNER_WIDTH = 1f;
    public static final float RUNNER_HEIGHT = 1.1f;
    public static final float RUNNER_X = 0;
    public static final float RUNNER_Y = GROUND_Y + GROUND_HEIGHT / 2 + RUNNER_HEIGHT / 2 + 0.1f;

    public static final float RUNNER_DENSITY = 1f;
    public static final Vector2 RUNNER_JUMPING_LINEAR_IMPULSE = new Vector2(0, 8f);
    public static final Vector2 RUNNER_RIGHT_LINEAR_IMPULSE = new Vector2(-5f, 0);
    public static final Vector2 RUNNER_LEFT_LINEAR_IMPULSE = new Vector2(5f, 0);
    public static final float RUNNER_FRICTION = 10f;
    public static final float RUNNER_SPEED = 5f;
    public static final float RUNNER_HIT_ANGULAR_IMPULSE = 10f;

    public static final float RUNNER_SHIFT_BULLET_X = 1f;
    public static final float RUNNER_SHIFT_BULLET_Y = 0.45f;

    public static final float RUNNER_MAX_GOD_MODE_TIME = 2f;
//    public static final float RUNNER_MAX_GOD_MODE_TIME = 0f;

    public static final int START_LIVES_COUNT = 2;

    public static final float BULLET_WIDTH = 0.1f;
    public static final float BULLET_HEIGHT = 0.1f;
    public static float BULLET_DENSITY = 0.5f;
    public static float BULLET_LINEAR_SPEED = 0.2f;

    public static final float POISON_BLAST_WIDTH = 0.3f;
    public static final float POISON_BLAST_HEIGHT = 0.3f;

    public static final float POISON_BLAST_SHIFT_BULLET_X = 1f;
    public static final float POISON_BLAST_SHIFT_BULLET_Y = 1.1f;

    public static final float WALL_WIDTH = 2f;
    public static final float WALL_HEIGHT = 15f;

    public static final float ENEMY_WIDTH = 1f;
    public static final float ENEMY_HEIGHT = 1f;
    public static final float ENEMY_DENSITY = 0.5f;
    public static final float ENEMY_FRICTION = 10f;
    public static final float ENEMY_SPEED = 5f;
    public static final Vector2 ENEMY_JUMPING_LINEAR_IMPULSE = new Vector2(0, 8f);

    public static final float ENEMY_DRAW_WIDTH = 128f;
    public static final float ENEMY_DRAW_HEIGHT = 128f;

    public static final float RABBIT_WIDTH = 0.5f;
    public static final float RABBIT_HEIGHT = 0.5f;
    public static final float RABBIT_DENSITY = 1f;
    public static final Vector2 RABBIT_JUMPING_LINEAR_IMPULSE_LEFT = new Vector2(-0.6f, 2f);
    public static final Vector2 RABBIT_JUMPING_LINEAR_IMPULSE_RIGHT = new Vector2(0.6f, 2f);
    public static final float RABBIT_WAITING_TIME = 2f;


    public static final int LARRY_LIFE_COUNT = 20;
    public static final float LARRY_WIDTH = 1.4f;
    public static final float LARRY_HEIGHT = 1.1f;
    public static final float LARRY_WAIT_TIME = 1f;
    public static final float LARRY_CHARGING_TIME = 1.5f;
    public static final float LARRY_OPEN_MOTH_TIME = 1.5f;

    public static final float BUTTON_STNDRT_SIZE = 128f;

    public static final float BUTTON_TITLE_WIDTH = 488f;
    public static final float BUTTON_TITLE_HEIGHT = 64f;

    public static final float BUTTON_PANEL_WIDTH = 124;
    public static final float BUTTON_PANEL_HEIGHT = 104f;


    public static final Vector2 CAT_SHOP_PANEL_POSITION = new Vector2(760f, 320f);
    public static final float BUTTON_CAT_SHOP_WIDTH = 112;
    public static final float BUTTON_CAT_SHOP_HEIGHT = 80f;

    public static final Vector2 SAD_CAT_POSITION = new Vector2(532f, 304f);
    public static final float SAD_CAT_WIDTH = 264f;
    public static final float SAD_CAT_HEIGHT = 300f;
    public static final float SAD_CAT_BLINK_PAUSE = 1f;


    public static final String BACKGROUND_IMAGE_PATH = "background.png";
    public static final String BACKGROUND_SHADOW_IMAGE_PATH = "background_shadow.png";

    public static final float START_X_LEFT_BUTTONS_GROUP = -600;
    public static final float START_X_RIGHT_BUTTONS_GROUP = 240;
    public static final float SPACE_Y_BUTTONS_GROUP = 70;

    public static final Vector2 LIFEBIRD_START_POSITION = new Vector2(-133, 705);
    public static final float LIFRBIRD_LIFE_TIME = 6f;

    public static final Vector2 MUSICBIRD_START_POSITION = new Vector2(-540, 640);

    public static final float PANEL_WIDTH = 420f;
    public static final float PANEL_HEIGHT = 320f;
    public static final float CAT_PANEL_WIDTH = 420f;
    public static final float CAT_PANEL_HEIGHT = 320f;



    public static final String BEST_HUNTER_LEADERBOARD = "system.leaderboard.bestHunter";
    public static final String KILL_DEERS_LEADERBOARD = "system.leaderboard.killedDeers";
    public static final String KILL_10 = "system.achievemets.kill10";
    public static final String KILL_50 = "system.achievemets.kill50";
    public static final String KILL_65 = "system.achievemets.kill65";
    public static final String KILL_100 = "system.achievemets.kill100";
    public static final String KILL_BIRD = "system.achievemets.killBird";

    public static final String CREDITS_VALUE = "system.credits.all";


    public static final String PREF_NAME = "HUNTER_PREF";
    public static final String PREF_MUSIC_ENABLED = "music_enabled";
    public static final String PREF_NOT_FIRST_LAUNCH = "not_first_launch";

    public static final String SKU_BEER = "cat_drink";
    public static final String SKU_SALAD = "cat_salad";
    public static final String SKU_MEAT = "cat_meat";
}
