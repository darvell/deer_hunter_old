package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.audio.Sound;

public class SoundPlayer {

    private Sound shot;
    private Sound jumpPlayer;
    private Sound landing;
    private Sound deerHit;
    private Sound birdFall;
    private Sound birdPickup;
    private Sound runningSound;
    private Sound splash;
    private Sound vomit;
    private Sound larryRoar;
    private Sound larryLaught;
    private Sound rabbitJump;


    public SoundPlayer(){
        reload();
    }

    public void playShot(){
        shot.play(0.7f);
    }

    public void playJumpPlayer(){
        jumpPlayer.play();
    }

    public void playLanding(){
        landing.play();
    }

    public void playHitDeer(){
        deerHit.play(0.6f);
    }

    public void playBirdFall(){
        birdFall.play(0.5f);
    }

    public void playBirdPickup(){
        birdPickup.play();
    }

    public void playSplash(){splash.play();}

    public void playVomit() {
        vomit.play();
    }

    public void playLarryRoar(){
        larryRoar.play();
    }

    public void playLarryLaught(){
        larryLaught.play();
    }

    public void reload(){
//        Assets.reloadSounds();
        shot = Assets.getSound(Assets.SOUND_SHOT2);
        jumpPlayer = Assets.getSound(Assets.SOUND_JUMP);
        landing = Assets.getSound(Assets.SOUND_LANDING);
        deerHit = Assets.getSound(Assets.SOUND_DEER_HIT);
        birdFall = Assets.getSound(Assets.SOUND_BIRD_FALL);
        birdPickup = Assets.getSound(Assets.SOUND_BIRD_PICKUP);
        runningSound = Assets.getSound(Assets.SOUND_RUNNING);
        splash = Assets.getSound(Assets.SOUND_SPLASH);
        vomit = Assets.getSound(Assets.SOUND_VOMIT);
        larryRoar = Assets.getSound(Assets.SOUND_LARRY_ROAR);
        larryLaught = Assets.getSound(Assets.SOUND_LARRY_LAUGHT);
        rabbitJump = Assets.getSound(Assets.SOUND_RABBIT_JUMP);
    }

    public long playRunningDeer(){
        long id = runningSound.play(1f);
        runningSound.setLooping(id, true);
        return id;
    }

    public void stopRunningDeer(long id){
        runningSound.stop(id);
    }

    public void playRabbitJump(){
        rabbitJump.play();
    }
}
