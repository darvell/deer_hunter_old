package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import ru.darvell.android.dhunterreal.actors.Player;
import ru.darvell.android.dhunterreal.actors.ThinLarry;
import ru.darvell.android.dhunterreal.box2d.*;
import ru.darvell.android.dhunterreal.enums.Direction;

public class WorldUtils {

    public static World createWorld() {
        return new World(Constants.WORLD_GRAVITY, true);
    }

    public static Body createGround(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(new Vector2(Constants.GROUND_X, Constants.GROUND_Y));
        Body body = world.createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.GROUND_WIDTH / 2, Constants.GROUND_HEIGHT / 2);
        body.createFixture(shape, Constants.GROUND_DENSITY);
        body.setUserData(new GroundUserData());
        shape.dispose();
        return body;
    }

    public static Body createWorldSensor(World world, Vector2 groundPosition, boolean right) {
        BodyDef bodyDef = new BodyDef();

        if (right) {
            groundPosition.x += Constants.GROUND_WIDTH / 2;
        } else {
            groundPosition.x -= Constants.GROUND_WIDTH / 2;
        }
        groundPosition.y += Constants.GROUND_HEIGHT;
        bodyDef.position.set(groundPosition);

        Body body = world.createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.WORLD_SENSOR_WIDTH / 2, Constants.ENEMY_HEIGHT / 2);
        Fixture fixture = body.createFixture(shape, Constants.GROUND_DENSITY);
//        fixture.setSensor(true);
        body.setUserData(new WorldSensorUserData());
        shape.dispose();
        return body;
    }

    public static Body createCenterWorldSensor(World world, Vector2 groundPosition) {
        BodyDef bodyDef = new BodyDef();

        groundPosition.y += Constants.GROUND_HEIGHT;
        bodyDef.position.set(groundPosition);

        Body body = world.createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.WORLD_SENSOR_WIDTH / 2, Constants.WORLD_SENSOR_WIDTH / 2);
        Fixture fixture = body.createFixture(shape, Constants.GROUND_DENSITY);
//        fixture.setSensor(true);
        body.setUserData(new CenterSensorUserData());
        shape.dispose();
        return body;
    }

    public static Body createLeftWall(World world, Vector2 groundPosition, boolean right) {
        BodyDef bodyDef = new BodyDef();
        if (right) {
            groundPosition.x += Constants.GROUND_WIDTH / 2 - Constants.WALL_WIDTH / 2;
        } else {
            groundPosition.x -= Constants.GROUND_WIDTH / 2 - Constants.WALL_WIDTH / 2;
        }
        groundPosition.y += Constants.GROUND_HEIGHT / 2 + Constants.WALL_HEIGHT / 2 + Constants.ENEMY_HEIGHT + 0.1;
        bodyDef.position.set(groundPosition);
        Body body = world.createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.WALL_WIDTH / 2, Constants.WALL_HEIGHT / 2);
        body.createFixture(shape, Constants.GROUND_DENSITY);
        body.setUserData(new WallUserData());
        shape.dispose();
        return body;
    }

    public static Body createEnemy(World world, Vector2 groundPosition, boolean right) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        if (right) {
            groundPosition.x += Constants.GROUND_WIDTH / 2 - Constants.WALL_WIDTH - Constants.ENEMY_WIDTH;
//            groundPosition.x += 2;
        } else {
            groundPosition.x -= Constants.GROUND_WIDTH / 2 - Constants.WALL_WIDTH - Constants.ENEMY_WIDTH;
//            groundPosition.x -= 2;
        }
        groundPosition.y += Constants.GROUND_HEIGHT / 2 + Constants.ENEMY_HEIGHT + 1;
        bodyDef.position.set(groundPosition);
        Body body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.ENEMY_WIDTH / 2, Constants.ENEMY_HEIGHT / 2);
        Fixture fixture = body.createFixture(shape, Constants.ENEMY_DENSITY);
        fixture.setFriction(Constants.ENEMY_FRICTION);

        PolygonShape shapeHead = new PolygonShape();
        if (right) {
            shapeHead.setAsBox(0.25f, 0.25f, new Vector2(-Constants.ENEMY_WIDTH / 2 + 0.25f, Constants.ENEMY_HEIGHT - 0.25f), 0);
        } else {
            shapeHead.setAsBox(0.25f, 0.25f, new Vector2(Constants.ENEMY_WIDTH / 2 - 0.25f, Constants.ENEMY_HEIGHT - 0.25f), 0);
        }
        Fixture headFixture = body.createFixture(shapeHead, 0);

//        fixture.setSensor(true);
        body.setFixedRotation(true);
        if (right) {
            body.setUserData(new DeerUserData(Constants.ENEMY_WIDTH, Constants.ENEMY_HEIGHT, Direction.LEFT, fixture, headFixture));
        } else {
            body.setUserData(new DeerUserData(Constants.ENEMY_WIDTH, Constants.ENEMY_HEIGHT, Direction.RIGHT, fixture, headFixture));
        }


        shape.dispose();
        shapeHead.dispose();
        return body;
    }


    public static Body createRabbit(World world, Vector2 groundPosition, boolean right){
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        if (right) {
            groundPosition.x += Constants.GROUND_WIDTH / 2 - Constants.WALL_WIDTH - Constants.RABBIT_WIDTH - 1f;
//            groundPosition.x += 2;
        } else {
            groundPosition.x -= Constants.GROUND_WIDTH / 2 - Constants.WALL_WIDTH - Constants.RABBIT_WIDTH - 1f;
//            groundPosition.x -= 2;
        }
        groundPosition.y += Constants.GROUND_HEIGHT / 2 + Constants.RABBIT_HEIGHT + 1;
        bodyDef.position.set(groundPosition);
        Body body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.RABBIT_WIDTH / 2, Constants.RABBIT_HEIGHT / 2);
        Fixture fixture = body.createFixture(shape, Constants.RABBIT_DENSITY);
        fixture.setFriction(Constants.ENEMY_FRICTION);


        body.setFixedRotation(true);
        if (right) {
            body.setUserData(new RabbitUserData(Constants.RABBIT_WIDTH, Constants.RABBIT_HEIGHT, Direction.LEFT, fixture));
        } else {
            body.setUserData(new RabbitUserData(Constants.RABBIT_WIDTH, Constants.RABBIT_HEIGHT, Direction.RIGHT, fixture));
        }


        shape.dispose();
        return body;
    }

    public static Body createThinLarry(World world, Vector2 groundPosition, boolean right) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        if (right) {
            groundPosition.x += Constants.GROUND_WIDTH / 2 - Constants.WALL_WIDTH - Constants.LARRY_WIDTH;
//            groundPosition.x += 2;
        } else {
            groundPosition.x -= Constants.GROUND_WIDTH / 2 - Constants.WALL_WIDTH - Constants.LARRY_WIDTH;
//            groundPosition.x -= 2;
        }

        groundPosition.y += Constants.GROUND_HEIGHT / 2 + Constants.LARRY_HEIGHT + 1;
        bodyDef.position.set(groundPosition);
        Body body = world.createBody(bodyDef);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.LARRY_WIDTH / 2, Constants.LARRY_HEIGHT / 2);
        Fixture fixture = body.createFixture(shape, Constants.ENEMY_DENSITY);
        fixture.setFriction(Constants.ENEMY_FRICTION);

        PolygonShape shapeHead = new PolygonShape();
        if (right) {
            shapeHead.setAsBox(0.25f, 0.4f, new Vector2(-Constants.LARRY_WIDTH / 2 + 0.25f, Constants.LARRY_HEIGHT / 2 + 0.4f), 0);
        } else {
            shapeHead.setAsBox(0.25f, 0.04f, new Vector2(Constants.LARRY_WIDTH / 2 - 0.25f, Constants.LARRY_HEIGHT / 2 + 0.4f), 0);
        }
        Fixture headFixture = body.createFixture(shapeHead, 0);

//        fixture.setSensor(true);
        body.setFixedRotation(true);
        if (right) {
            body.setUserData(new ThinLarryUserData(Constants.LARRY_WIDTH, Constants.LARRY_HEIGHT, Direction.LEFT, fixture, headFixture));
        } else {
            body.setUserData(new ThinLarryUserData(Constants.LARRY_WIDTH, Constants.LARRY_HEIGHT, Direction.RIGHT, fixture, headFixture));
        }


        shape.dispose();
        shapeHead.dispose();
        return body;
    }

    public static Body createPlayer(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(Constants.RUNNER_X, Constants.RUNNER_Y));

        Body body = world.createBody(bodyDef);

        body.setFixedRotation(true);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.RUNNER_WIDTH / 2, Constants.RUNNER_HEIGHT / 2);
        Fixture playerBodyFixture = body.createFixture(shape, Constants.RUNNER_DENSITY);


//        CircleShape circle = new CircleShape();
//        circle.setRadius(Constants.RUNNER_WIDTH / 2);
//        circle.setPosition(new Vector2(0, -Constants.RUNNER_HEIGHT / 2));
//        Fixture playerSensorFixture = body.createFixture(circle, 1);
//        circle.dispose();

        playerBodyFixture.setFriction(Constants.RUNNER_FRICTION);
//        playerSensorFixture.setFriction(Constants.RUNNER_FRICTION);
//        playerSensorFixture.setSensor(true);
        body.setUserData(new PlayerUserData(Constants.RUNNER_WIDTH, Constants.RUNNER_HEIGHT, playerBodyFixture));


        shape.dispose();
        return body;
    }

    public static Body createBullet(World world, Player player) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(calculateBulletStartPosition(player));

        Body body = world.createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(Constants.BULLET_WIDTH / 2, Constants.BULLET_HEIGHT / 2);
        body.createFixture(shape, Constants.BULLET_DENSITY);
        shape.dispose();
        body.setUserData(new BulletUserData(Constants.BULLET_WIDTH, Constants.BULLET_HEIGHT, player.getDirection()));
        body.setBullet(true);
        return body;
    }

    public static Body createBird(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(MathUtils.random(-9f, 9f),25));

        Body body = world.createBody(bodyDef);

//        CircleShape circle = new CircleShape();
//        circle.setRadius(0.6f / 2);
//        circle.setPosition(bodyDef.position);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(0.6f / 2, 0.6f / 2);
        Fixture f = body.createFixture(shape, Constants.BULLET_DENSITY);
        f.setRestitution(0.05f);
        f.setDensity(1f);
        f.setFriction(10f);

        shape.dispose();

        body.setUserData(new FallenBirdUserData(0.6f, 0.6f));
        body.setBullet(true);
        return body;
    }

    public static Array<Body> createPoisonBlast(World world, int count, ThinLarry thinLarry, boolean center) {
        Array<Body> bodies = new Array<Body>(count);

        for (int i = 0; i < count; i++) {
            BodyDef bodyDef = new BodyDef();
            bodyDef.type = BodyDef.BodyType.DynamicBody;
            Vector2 startPosition = null;
            if (center) {
                startPosition = calculateCenterPoisonBlastStartPosition(thinLarry);
            } else {
                startPosition = calculatePoisonBlastStartPosition(thinLarry);
            }
            if (startPosition == null){
                return new Array<Body>();
            }

            bodyDef.position.set(startPosition);

            Body body = world.createBody(bodyDef);
            PolygonShape shape = new PolygonShape();
            shape.setAsBox(Constants.POISON_BLAST_WIDTH / 2, Constants.POISON_BLAST_HEIGHT / 2);
            Fixture fixture = body.createFixture(shape, Constants.BULLET_DENSITY);
            fixture.setFriction(10f);
            shape.dispose();
            body.setUserData(new PoisonBlastUserData(Constants.POISON_BLAST_WIDTH, Constants.POISON_BLAST_HEIGHT, thinLarry.getDirection()));
            body.setBullet(true);

            bodies.add(body);
        }
        return bodies;
    }

    public static Vector2 calculateBulletStartPosition(Player player) {
        Vector2 vector = new Vector2();
        vector.y = player.getPosition().y + Constants.RUNNER_SHIFT_BULLET_Y;
        if (player.getDirection().equals(Direction.RIGHT)) {
            vector.x = player.getPosition().x + Constants.RUNNER_SHIFT_BULLET_X;
        } else {
            vector.x = player.getPosition().x - Constants.RUNNER_SHIFT_BULLET_X;
        }
        return vector;
    }

    public static Vector2 calculatePoisonBlastStartPosition(ThinLarry thinLarry){
        Vector2 vector = new Vector2();
        if (thinLarry != null) {
            vector.y = thinLarry.getPosition().y + Constants.POISON_BLAST_SHIFT_BULLET_Y;
            if (thinLarry.getDirection().equals(Direction.RIGHT)) {
                vector.x = thinLarry.getPosition().x + Constants.POISON_BLAST_SHIFT_BULLET_X;
            } else {
                vector.x = thinLarry.getPosition().x - Constants.POISON_BLAST_SHIFT_BULLET_X;
            }
            return vector;
        }else {
            return null;
        }

    }

    public static Vector2 calculateCenterPoisonBlastStartPosition(ThinLarry thinLarry){
        Vector2 vector = new Vector2();
        if (thinLarry != null) {
            vector.y = thinLarry.getPosition().y + Constants.POISON_BLAST_SHIFT_BULLET_Y + 1;
            if (thinLarry.getDirection().equals(Direction.RIGHT)) {
                vector.x = thinLarry.getPosition().x;
            } else {
                vector.x = thinLarry.getPosition().x;
            }
            return vector;
        }else {
            return null;
        }

    }
}
