package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.Gdx;

public class GraphicsUtils {

    public static float calculateUpperY(){
        return ((Constants.APP_HEIGHT - Gdx.graphics.getHeight()) / 2) + Gdx.graphics.getHeight();
    }

    public static float calculateButtomY(){
        return (Constants.APP_HEIGHT - Gdx.graphics.getHeight()) / 2;
    }

}
