package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

public abstract class BodyActorsUtils {

    World world;

    public BodyActorsUtils(World world) {
        this.world = world;
    }

    boolean checkAndDestroyBody(Body body) {
        if ((body != null) && (BodyUtils.bodyIsNeedDelete(body))) {
            destroyBody(body);
            return true;
        }
        return false;
    }

    void destroyBody(Body body) {
        body.setUserData(null);
        world.destroyBody(body);
    }
}
