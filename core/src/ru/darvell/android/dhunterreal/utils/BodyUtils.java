package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.physics.box2d.Body;
import ru.darvell.android.dhunterreal.box2d.UserData;
import ru.darvell.android.dhunterreal.enums.UserDataType;

public class BodyUtils {

    public static boolean bodyIsEnemy(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.ENEMY;
    }

    public static boolean bodyIsPlayer(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.PLAYER;
    }

    public static boolean bodyIsGround(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.GROUND;
    }

    public static boolean bodyIsNeedDelete(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.isNeedDelete();
    }

    public static boolean bodyIsBullet(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.BULLET;
    }

    public static boolean bodyIsThinLarry(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.THIN_LARRY;
    }

    public static boolean bodyIsDeadBullet(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.BULLET && userData.isNeedDelete();
    }

    public static boolean bodyIsDeadEnemy(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.ENEMY && userData.isNeedDelete();
    }

    public static boolean bodyIsDeadPlayer(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.PLAYER && userData.isNeedDelete();
    }

    public static boolean bodyIsWall(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.WALL;
    }

    public static boolean bodyIsWorldSensor(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.WORLD_SENSOR;
    }

    public static boolean bodyIsCenterSensor(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.CENTER_SENSOR;
    }

    public static boolean bodyIsPoisonBlast(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.POISON_BLAST;
    }

    public static boolean bodyIsFallenBird(Body body) {
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.FALLEN_BIRD;
    }

    public static boolean bodyIsRabbit(Body body){
        UserData userData = (UserData) body.getUserData();
        return userData != null && userData.getUserDataType() == UserDataType.RABBIT;
    }
}
