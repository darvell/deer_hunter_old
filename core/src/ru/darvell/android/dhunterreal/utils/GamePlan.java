package ru.darvell.android.dhunterreal.utils;

import ru.darvell.android.dhunterreal.ActionResolver;

public class GamePlan {

    public static final int KILL_TYPE_RABBIT = 1;
    public static final int KILL_TYPE_DEER = 2;
    protected int maxEnemiesOnLevel;
    private int killedDeerCount;
    private float timeToNextEnemy;
    private boolean needRabbit = false;
    private int maxRabbitsCount = 2;

    private int totalScore = 0;

    private boolean needBoss = false;
    private boolean bossState = false;

    private ActionResolver actionResolver;

    public GamePlan(ActionResolver actionResolver) {
        this.actionResolver = actionResolver;
        killedDeerCount = 0;
        timeToNextEnemy = 2f;
        maxEnemiesOnLevel = 1;
    }

    public void addKill(int type) {
        killedDeerCount++;
        updateGameProcessParameters();
        switch (type) {
            case KILL_TYPE_DEER:
                totalScore += 3;
                break;
            case KILL_TYPE_RABBIT:
                totalScore += 5;
                break;
        }

        checkAchievement();
    }

    private void checkAchievement() {
        if (actionResolver != null) {
            if (killedDeerCount == 10) {
                actionResolver.unlockAchievement(Assets.getBundle(Assets.STRINGS).get(Constants.KILL_10));
            }
            if (killedDeerCount == 50) {
                actionResolver.unlockAchievement(Assets.getBundle(Assets.STRINGS).get(Constants.KILL_50));
            }
            if (killedDeerCount == 65) {
                actionResolver.unlockAchievement(Assets.getBundle(Assets.STRINGS).get(Constants.KILL_65));
            }
            if (killedDeerCount == 100) {
                actionResolver.unlockAchievement(Assets.getBundle(Assets.STRINGS).get(Constants.KILL_100));
            }
        }
    }

    public void killLarry() {
        totalScore += 20;
    }

    public void killBird() {
        actionResolver.unlockAchievement(Assets.getBundle(Assets.STRINGS).get(Constants.KILL_BIRD));
    }

    protected void updateGameProcessParameters() {
        if (((killedDeerCount >= 5) && (killedDeerCount <= 10))) {
            timeToNextEnemy = 1f;
            maxEnemiesOnLevel = 2;
        } else if (((killedDeerCount > 10) && (killedDeerCount <= 15))) {
            timeToNextEnemy = 1.5f;
            maxEnemiesOnLevel = 3;
        } else if (((killedDeerCount > 15) && (killedDeerCount <= 25))) {
            timeToNextEnemy = 1f;
            maxEnemiesOnLevel = 3;
        } else if (killedDeerCount == 28) {
            setBossState(true);
            setNeedBoss(true);
        } else if (((killedDeerCount > 28) && (killedDeerCount <= 45))) {
            timeToNextEnemy = 1f;
            maxEnemiesOnLevel = 3;
        } else if (((killedDeerCount > 45) && (killedDeerCount <= 60))) {
            timeToNextEnemy = 1.5f;
            maxEnemiesOnLevel = 4;
        } else if (killedDeerCount == 61) {
            setBossState(true);
            setNeedBoss(true);
        }
//        if (killedDeerCount  5)
        if ((killedDeerCount % 5) == 0) {
            needRabbit = true;
        }
    }

    public float getTimeToNextEnemy() {
        return timeToNextEnemy;
    }

    public int getMaxDeersOnLevel() {
        return maxEnemiesOnLevel;
    }

    public int getKilledDeerCount() {
        return killedDeerCount;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public boolean isNeedBoss() {
        return needBoss;
    }

    public void setNeedBoss(boolean needBoss) {
        this.needBoss = needBoss;
    }

    public boolean isBossState() {
        return bossState;
    }

    public void setBossState(boolean bossState) {
        this.bossState = bossState;
    }

    public void shoot() {
        totalScore -= 1;
        if (totalScore < 0) {
            totalScore = 0;
        }
    }

    public void sendLeaderBoard() {
        if (actionResolver != null) {
            actionResolver.submitScore(
                    Assets.getBundle(Assets.STRINGS).get(Constants.BEST_HUNTER_LEADERBOARD),
                    getTotalScore()
            );
            actionResolver.submitScore(
                    Assets.getBundle(Assets.STRINGS).get(Constants.KILL_DEERS_LEADERBOARD),
                    getKilledDeerCount()
            );
        }
    }

    public void addPointsForBird() {
        totalScore += 3;
    }

    public boolean isNeedRabbit() {
        return needRabbit;
    }

    public void setNeedRabbit(boolean needRabbit) {
        this.needRabbit = needRabbit;
    }

    public int getMaxRabbitsCount() {
        return maxRabbitsCount;
    }
}
