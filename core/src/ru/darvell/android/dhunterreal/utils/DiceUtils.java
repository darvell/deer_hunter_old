package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.math.MathUtils;

public class DiceUtils {


    public static boolean throwDices(int diceCount, int winCount) {
        int winThrows = 0;
        for (int i = 0; i < diceCount; i++){
            int throwResult = MathUtils.random(1,6);
            if (checkWin(throwResult)){
                winThrows++;
            }
            if (winThrows == winCount){
                return true;
            }
        }
        return false;
    }

    public static boolean checkWin(int number){
        if ((number == 5) || (number == 6)){
            return true;
        } else {
            return false;
        }
    }

}
