package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import ru.darvell.android.dhunterreal.actors.PoisonBlast;
import ru.darvell.android.dhunterreal.actors.ThinLarry;
import ru.darvell.android.dhunterreal.actors.interfaces.AnyBoodies;

public class BulletUtils {

    public static void createPoisonBlast(World world, Stage stage, ThinLarry thinLarry, Array<AnyBoodies> poisonBlastArray) {
        Array<Body> bodies = WorldUtils.createPoisonBlast(world, 5, thinLarry, false);
        for (int i = 0; i < bodies.size; i++){
            PoisonBlast poisonBlast = new PoisonBlast(bodies.get(i), false);
            poisonBlastArray.add(poisonBlast);
            stage.addActor(poisonBlast);
        }

    }

    public static void createCenterPoisonBlast(World world, Stage stage, ThinLarry thinLarry, Array<AnyBoodies> poisonBlastArray) {
        Array<Body> bodies = WorldUtils.createPoisonBlast(world, 7, thinLarry, true);
        for (int i = 0; i < bodies.size; i++){
            PoisonBlast poisonBlast = new PoisonBlast(bodies.get(i), true);
            poisonBlastArray.add(poisonBlast);
            stage.addActor(poisonBlast);
        }

    }

}
