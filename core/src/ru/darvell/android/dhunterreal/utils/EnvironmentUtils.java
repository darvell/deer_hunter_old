package ru.darvell.android.dhunterreal.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import ru.darvell.android.dhunterreal.actors.*;
import ru.darvell.android.dhunterreal.actors.interfaces.AnyBoodies;
import ru.darvell.android.dhunterreal.screens.ParentScreen;

public class EnvironmentUtils extends BodyActorsUtils {

    private ParentScreen parentScreen;
    private GamePlan gamePlan;

    private Array<AnyBoodies> boodies = new Array();

    public EnvironmentUtils(ParentScreen parentScreen, GamePlan gamePlan, World world) {
        super(world);
        this.gamePlan = gamePlan;
        this.parentScreen = parentScreen;
    }

    public void createBullet(Stage stage, Player player) {
        Bullet bullet = new Bullet(WorldUtils.createBullet(world, player));
        bullet.shoot();
        stage.addActor(bullet);
        boodies.add(bullet);
        if (gamePlan != null) {
            gamePlan.shoot();
        }
        parentScreen.getSoundPlayer().playShot();
    }

    public void createPoisonBlast(Stage stage, ThinLarry thinLarry, boolean center) {
        if (center) {
            BulletUtils.createCenterPoisonBlast(world, stage, thinLarry, boodies);
        } else {
            BulletUtils.createPoisonBlast(world, stage, thinLarry, boodies);
        }
        parentScreen.getSoundPlayer().playVomit();
    }

    public void createFallenBird(Stage stage) {
        boolean hasFallenBird = false;
        for (AnyBoodies body : boodies) {
            if (body instanceof FallenLifeBird) {
                hasFallenBird = true;
            }
        }

        if (!hasFallenBird) {
            FallenLifeBird lifeBird = new FallenLifeBird(WorldUtils.createBird(world));
            stage.addActor(lifeBird);
            lifeBird.getBody().applyLinearImpulse(new Vector2(0, -20), lifeBird.getBody().getWorldCenter(), true);
            boodies.add(lifeBird);
        }
    }

    public void update(float delta) {
        for (AnyBoodies anyBoodie : boodies) {
            anyBoodie.update(delta);
        }
    }

    public void markAllEnviromentBodiesToDelete() {
        for (AnyBoodies body : boodies) {
            body.setNeedDelete(true);
        }
    }

    public FallenLifeBird getFallenLifeBird() {
        for (AnyBoodies b : boodies) {
            if (b instanceof FallenLifeBird) {
                return (FallenLifeBird) b;
            }
        }
        return null;
    }

    public void deleteAllMarkedBodies() {
        for (AnyBoodies actor : boodies) {
            if (actor instanceof FallenLifeBird) {
                FallenLifeBird lifeBird = (FallenLifeBird) actor;
                if (checkAndDestroyBody(lifeBird.getBody())) {
                    lifeBird.resetBody();
                    boodies.removeValue(lifeBird, true);
                }
            }

            if (actor instanceof Bullet) {
                Bullet bullet = (Bullet) actor;
                if (checkAndDestroyBody(bullet.getBody())) {
                    bullet.resetBody();
                    boodies.removeValue(bullet, true);
                }
            }

            if (actor instanceof PoisonBlast) {
                PoisonBlast poisonBlast = (PoisonBlast) actor;
                if (checkAndDestroyBody(poisonBlast.getBody())) {
                    boodies.removeValue(poisonBlast, true);
                    poisonBlast.resetBody();
                }
            }
        }
    }


}
