package ru.darvell.android.dhunterreal.box2d;

import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.enums.UserDataType;

public class PoisonBlastUserData extends UserData {

    private float speed;
    private Direction direction;
    boolean onGround = false;

    public PoisonBlastUserData(float width, float height, Direction direction) {
        super(width, height);
        userDataType = UserDataType.POISON_BLAST;
        this.direction = direction;
    }

    public boolean isOnGround() {
        return onGround;
    }

    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
