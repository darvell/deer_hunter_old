package ru.darvell.android.dhunterreal.box2d;

import ru.darvell.android.dhunterreal.enums.UserDataType;

public class GroundUserData extends UserData{


    public GroundUserData() {
        super();
        userDataType = UserDataType.GROUND;
    }
}
