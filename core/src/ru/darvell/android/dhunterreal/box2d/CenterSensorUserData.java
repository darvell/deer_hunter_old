package ru.darvell.android.dhunterreal.box2d;

import ru.darvell.android.dhunterreal.enums.UserDataType;

public class CenterSensorUserData extends UserData{

    public CenterSensorUserData() {
        super();
        userDataType = UserDataType.CENTER_SENSOR;
    }

}
