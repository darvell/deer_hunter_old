package ru.darvell.android.dhunterreal.box2d;

import ru.darvell.android.dhunterreal.enums.UserDataType;

public class UserData {

    protected UserDataType userDataType;
    protected float width;
    protected float height;

    protected boolean needDelete;

    public UserData() {
    }

    public UserData(float width, float height) {
        this.width = width;
        this.height = height;
    }

    public UserDataType getUserDataType() {
        return userDataType;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public boolean isNeedDelete() {
        return needDelete;
    }

    public void setNeedDelete(boolean needDelete) {
        this.needDelete = needDelete;
    }
}
