package ru.darvell.android.dhunterreal.box2d;

import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.enums.UserDataType;
import ru.darvell.android.dhunterreal.utils.Constants;

public class BulletUserData extends UserData {

    private Direction direction;
    private float bulletSpeed;

    public BulletUserData(float width, float height, Direction direction) {
        super(width, height);
        userDataType = UserDataType.BULLET;
        this.direction = direction;
        bulletSpeed = Constants.BULLET_LINEAR_SPEED;
    }

    public Direction getDirection() {
        return direction;
    }

    public float getBulletSpeed() {
        return bulletSpeed;
    }
}
