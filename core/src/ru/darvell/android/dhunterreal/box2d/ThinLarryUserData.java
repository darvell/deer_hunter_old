package ru.darvell.android.dhunterreal.box2d;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Fixture;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.enums.UserDataType;
import ru.darvell.android.dhunterreal.utils.Constants;

public class ThinLarryUserData extends UserData {

    private Direction direction;
    private float speed;
    private float friction;
    private boolean needChangeDirection;
    private boolean needJump;
    private Fixture fixture;
    private Fixture headFixture;


    public ThinLarryUserData(float width, float height, Direction direction, Fixture fixture, Fixture headFixture) {
        super(width, height);
        this.fixture = fixture;
        this.headFixture = headFixture;
        userDataType = UserDataType.THIN_LARRY;
        this.direction = direction;
        speed = MathUtils.random(3.5f, Constants.ENEMY_SPEED);
        friction = Constants.ENEMY_FRICTION;
    }

    public Direction getDirection() {
        return direction;
    }

    public float getSpeed() {
        return speed;
    }

    public float getFriction() {
        return friction;
    }

    public boolean isNeedChangeDirection() {
        return needChangeDirection;
    }

    public boolean isNeedJump() {
        return needJump;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setFriction(float friction) {
        this.friction = friction;
    }

    public void setNeedChangeDirection(boolean needChangeDirection) {
        this.needChangeDirection = needChangeDirection;
    }

    public void setNeedJump(boolean needJump) {
        this.needJump = needJump;
    }

    public Fixture getFixture() {
        return fixture;
    }

    public Fixture getHeadFixture() {
        return headFixture;
    }
}
