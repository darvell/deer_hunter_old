package ru.darvell.android.dhunterreal.box2d;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.enums.UserDataType;
import ru.darvell.android.dhunterreal.utils.Constants;

public class DeerUserData extends UserData {

    private Direction direction;
    private float speed;
    private float friction;
    private boolean needChangeDirection;
    private boolean needJump;
    private Fixture fixture;
    private Fixture headFixture;


    private Vector2 jumpingLinearImpulse;

    public DeerUserData(float width, float height, Direction direction, Fixture fixture, Fixture headFixture) {
        super(width, height);
        this.fixture = fixture;
        this.headFixture = headFixture;
        userDataType = UserDataType.ENEMY;
        this.direction = direction;
        speed = MathUtils.random(3.5f, Constants.ENEMY_SPEED);
        friction = Constants.ENEMY_FRICTION;
        jumpingLinearImpulse = Constants.ENEMY_JUMPING_LINEAR_IMPULSE;
    }

    public float getSpeed() {
        return speed;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction){
        this.direction = direction;
    }

    public void setNeedChangeDirection(boolean needChange){
        this.needChangeDirection = needChange;
    }

    public boolean isNeedChangeDirection() {
        return needChangeDirection;
    }

    public Fixture getFixture() {
        return fixture;
    }

    public Fixture getHeadFixture() {
        return headFixture;
    }

    public boolean isNeedJump() {
        return needJump;
    }

    public void setNeedJump(boolean needJump) {
        this.needJump = needJump;
    }

    public float getFriction() {
        return friction;
    }

    public Vector2 getJumpingLinearImpulse() {
        return jumpingLinearImpulse;
    }


}
