package ru.darvell.android.dhunterreal.box2d;

import ru.darvell.android.dhunterreal.enums.UserDataType;

public class FallenBirdUserData extends UserData{

    public FallenBirdUserData(float width, float height) {
        super(width, height);
        userDataType = UserDataType.FALLEN_BIRD;
    }
}
