package ru.darvell.android.dhunterreal.box2d;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.enums.UserDataType;
import ru.darvell.android.dhunterreal.utils.Constants;

public class RabbitUserData extends UserData {

    private Direction direction;
    private float speed;
    private float friction;
    private boolean needChangeDirection;
    private boolean needJump;
    private Fixture fixture;
    private boolean landed;

    private Vector2 jumpingLinearImpulseLeft;
    private Vector2 jumpingLinearImpulseRight;

    public RabbitUserData(float width, float height, Direction direction, Fixture fixture) {
        super(width, height);
        this.fixture = fixture;
        userDataType = UserDataType.RABBIT;
        this.direction = direction;
        speed = MathUtils.random(3.5f, Constants.ENEMY_SPEED);
        friction = Constants.ENEMY_FRICTION;
        jumpingLinearImpulseLeft = Constants.RABBIT_JUMPING_LINEAR_IMPULSE_LEFT;
        jumpingLinearImpulseRight = Constants.RABBIT_JUMPING_LINEAR_IMPULSE_RIGHT;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getFriction() {
        return friction;
    }

    public void setFriction(float friction) {
        this.friction = friction;
    }

    public boolean isNeedChangeDirection() {
        return needChangeDirection;
    }

    public void setNeedChangeDirection(boolean needChangeDirection) {
        this.needChangeDirection = needChangeDirection;
    }

    public boolean isNeedJump() {
        return needJump;
    }

    public void setNeedJump(boolean needJump) {
        this.needJump = needJump;
    }

    public Fixture getFixture() {
        return fixture;
    }

    public void setFixture(Fixture fixture) {
        this.fixture = fixture;
    }

    public Vector2 getJumpingLinearImpulse() {
        return direction.equals(Direction.LEFT)?jumpingLinearImpulseLeft:jumpingLinearImpulseRight;
    }

    public boolean isLanded() {
        return landed;
    }

    public void setLanded(boolean landed) {
        this.landed = landed;
    }
}
