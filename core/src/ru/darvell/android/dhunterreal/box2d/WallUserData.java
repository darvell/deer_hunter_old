package ru.darvell.android.dhunterreal.box2d;

import ru.darvell.android.dhunterreal.enums.UserDataType;

public class WallUserData extends UserData{

    public WallUserData() {
        super();
        userDataType = UserDataType.WALL;
    }

}
