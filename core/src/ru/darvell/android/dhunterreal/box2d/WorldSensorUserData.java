package ru.darvell.android.dhunterreal.box2d;

import ru.darvell.android.dhunterreal.enums.UserDataType;

public class WorldSensorUserData extends UserData {

    public WorldSensorUserData() {
        super();
        userDataType = UserDataType.WORLD_SENSOR;
    }
}
