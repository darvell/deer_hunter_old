package ru.darvell.android.dhunterreal.box2d;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import ru.darvell.android.dhunterreal.enums.UserDataType;
import ru.darvell.android.dhunterreal.utils.Constants;

public class PlayerUserData extends UserData{

    private Vector2 jumpingLinearImpulse;
    private Vector2 rightLinearImpulse;
    private Vector2 leftLinearImpulse;

    private Fixture bodyFixture;

    private float speed;
    private float friction;
    private float hitImpulse;

    public PlayerUserData(float width, float height, Fixture bodyFixture) {
        super(width, height);
        userDataType = UserDataType.PLAYER;
        jumpingLinearImpulse = Constants.RUNNER_JUMPING_LINEAR_IMPULSE;
        rightLinearImpulse = Constants.RUNNER_RIGHT_LINEAR_IMPULSE;
        leftLinearImpulse = Constants.RUNNER_LEFT_LINEAR_IMPULSE;

        friction = Constants.RUNNER_FRICTION;
        speed = Constants.RUNNER_SPEED;
        hitImpulse = Constants.RUNNER_HIT_ANGULAR_IMPULSE;

        this.bodyFixture = bodyFixture;
        userDataType = UserDataType.PLAYER;
    }

    public Vector2 getJumpingLinearImpulse() {
        return jumpingLinearImpulse;
    }

    public void setJumpingLinearImpulse(Vector2 jumpingLinearImpulse) {
        this.jumpingLinearImpulse = jumpingLinearImpulse;
    }

    public Vector2 getRightVelocity(){
        return rightLinearImpulse;
    }

    public Vector2 getLeftVelocity(){
        return leftLinearImpulse;
    }

    public Fixture getBodyFixture() {
        return bodyFixture;
    }

    public float getFriction() {
        return friction;
    }

    public float getSpeed() {
        return speed;
    }

    public float getHitImpulse() {
        return hitImpulse;
    }
}
