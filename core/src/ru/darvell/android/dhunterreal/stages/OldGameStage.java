package ru.darvell.android.dhunterreal.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import ru.darvell.android.dhunterreal.DHunterReal;
import ru.darvell.android.dhunterreal.actors.*;
import ru.darvell.android.dhunterreal.actors.interfaces.MusicBirdListener;
import ru.darvell.android.dhunterreal.actors.ui.*;
import ru.darvell.android.dhunterreal.box2d.*;
import ru.darvell.android.dhunterreal.enums.Direction;
import ru.darvell.android.dhunterreal.screens.ParentScreen;
import ru.darvell.android.dhunterreal.utils.*;

public class OldGameStage extends Stage implements ContactListener, MyButtonListener, InputProcessor, MusicBirdListener, MyResizibleStage {

    private static final String TAG = "GameStage";

    private static final int VIEWPORT_WIDTH = Constants.APP_WIDTH;
    private static final int VIEWPORT_HEIGHT = Constants.APP_HEIGHT;
    //            private static final float VIEWPORT_WIDTH = 19;
//    private static final float VIEWPORT_HEIGHT = 14.25f;
    private final float TIME_STEP = 1 / 300f;
    private float timeFromLastEnemy;

    private boolean inTimeout = true;
    private float timeout;

    private ParentScreen parentScreen;

    private OrthographicCamera camera;
    private Box2DDebugRenderer renderer;

    private World world;
    private Ground ground;
    private Player player;
    private LifeBird lifeBird;
    private MusicBird musicBird;
    private Panel panel;

    private boolean pauseWorld;
    private GamePlan gamePlan;

    private float accumulator = 0f;

    private Button btnLeft;
    private Button btnRight;
    private Button btnShoot;
    private Button btnJump;

    private Label killDeerLabel;

    private Array<DeadBody> deadBodies = new Array();


    private int bestScore = 0;

    private Button btnBack;
    private float bossTimeOut = 0;

    private boolean enemyCornerRight = true;

    private MobsUtils mobsUtils;
    private EnvironmentUtils envUtils;

    Touchpad touchpad;

    public OldGameStage(ParentScreen parentScreen) {
        super(new FillViewport(Constants.APP_WIDTH, Constants.APP_HEIGHT));
        this.parentScreen = parentScreen;
        setupCamera();
        getViewport().setCamera(camera);

        setupWorld();
        setupLabels();
        addActor(new BackgroundShadow());
        setupLifeBird();
        setupMusicBird();
        setupButtons();
        setupPanel();
//        panel.setVisible(true);
        reset();
        envUtils = new EnvironmentUtils(parentScreen, gamePlan, world);
        mobsUtils = new MobsUtils(parentScreen, gamePlan, world);

        setupPlayer();
//        mobsUtils.createRabbit(this, ground, enemyCornerRight);

//        createRabbit();
//        createBoss();

//        renderer = new Box2DDebugRenderer();

        Skin skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        touchpad = new Touchpad(10, skin);
        touchpad.setBounds(15, 150, 100, 100);
        addActor(touchpad);

    }

    public OldGameStage(Viewport viewport) {
        super(viewport);
        setupCamera();
        getViewport().setCamera(camera);
        setupButtons();
    }

    private void setupPanel() {
        panel = new Panel(VIEWPORT_WIDTH / 2, Constants.APP_HEIGHT / 2, this);
        addActor(panel);
//        panel.show();
    }

    private void setupButtons() {
        float yPosition = DHunterReal.MIN_Y + Constants.SPACE_Y_BUTTONS_GROUP;

        GdxLog.d(TAG, "Y position: " + yPosition);

        btnLeft = ButtonFabric.getButton(Constants.START_X_LEFT_BUTTONS_GROUP, yPosition, ButtonTypes.CONTROL_TYPE, ButtonActionTypes.LEFT, this);
        btnRight = ButtonFabric.getButton(btnLeft.getX() + btnLeft.getWidth() + 60, yPosition, ButtonTypes.CONTROL_TYPE, ButtonActionTypes.RIGHT, this);
        btnShoot = ButtonFabric.getButton(Constants.START_X_RIGHT_BUTTONS_GROUP, yPosition, ButtonTypes.CONTROL_TYPE, ButtonActionTypes.SHOOT, this);
        btnJump = ButtonFabric.getButton(btnShoot.getX() + btnShoot.getWidth() + 60, yPosition, ButtonTypes.CONTROL_TYPE, ButtonActionTypes.JUMP, this);
        addActor(btnLeft);
        addActor(btnRight);
        addActor(btnShoot);
        addActor(btnJump);

        btnBack = ButtonFabric.getButton(Constants.APP_WIDTH / 2 - Constants.BUTTON_PANEL_WIDTH - 20, DHunterReal.MAX_Y - Constants.BUTTON_PANEL_HEIGHT - 20, ButtonTypes.SERVICE_TYPE, ButtonActionTypes.BACK, this);
        addActor(btnBack);

//        Gdx.input.setInputProcessor(this);
    }

    private void resetButtonsPosition() {
        float yPosition = DHunterReal.MIN_Y + Constants.SPACE_Y_BUTTONS_GROUP;
        btnLeft.setY(yPosition);
        btnRight.setY(yPosition);
        btnShoot.setY(yPosition);
        btnJump.setY(yPosition);

        btnBack.setY(DHunterReal.MAX_Y - Constants.BUTTON_PANEL_HEIGHT - 20);
    }


//    private float calculateBottomStart() {
//        return (Constants.APP_HEIGHT - Gdx.graphics.getHeight()) / 2;
//    }

    private void setUpBackground() {
        addActor(new Background(0 - Constants.APP_WIDTH / 2, 0, Assets.getTexture(Assets.GAME_BACKGROUND)));
    }

    private void setupSensors() {
        WorldUtils.createWorldSensor(world, ground.getPosition(), true);
        WorldUtils.createWorldSensor(world, ground.getPosition(), false);
        WorldUtils.createCenterWorldSensor(world, ground.getPosition());
    }

    private void setupWorld() {
        world = WorldUtils.createWorld();
        world.setContactListener(this);
        setUpGround();
        setUpBackground();
        setupWalls();
        setupSensors();

        pauseWorld = false;
    }

    private void setupLabels() {
        Skin skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        String deerCount = "0";
        if (gamePlan != null) {
            deerCount = String.valueOf(gamePlan.getKilledDeerCount());
        }
        killDeerLabel = new Label(String.valueOf(deerCount), skin);
        killDeerLabel.setX(-133);
        killDeerLabel.setY(588);
        addActor(killDeerLabel);
    }


    private void setUpGround() {
        ground = new Ground(WorldUtils.createGround(world));
        addActor(ground);
    }

    private void setupLifeBird() {
        lifeBird = new LifeBird();
        addActor(lifeBird);
    }

    private void setupMusicBird() {
        musicBird = new MusicBird(this);
        musicBird.setPlayback(parentScreen.getMusicPlayer().isEnabled());
        addActor(musicBird);
    }

    private void setupPlayer() {
        player = new Player(WorldUtils.createPlayer(world), parentScreen.getSoundPlayer());
        addActor(player);
    }

    private void setupCamera() {
//        camera = new OrthographicCamera(VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
        camera = new OrthographicCamera(VIEWPORT_WIDTH, Gdx.graphics.getHeight());
//        camera.position.set(0, 0, 0f);
        camera.position.set(0, Constants.APP_HEIGHT / 2, 0f);
//        camera.position.set(0, 0, 0f);
//        camera.zoom = 0.05f;
        camera.update();
    }

    private void setupWalls() {
        addActor(new Wall(WorldUtils.createLeftWall(world, ground.getPosition(), false)));
        addActor(new Wall(WorldUtils.createLeftWall(world, ground.getPosition(), true)));
    }


    @Override
    public void draw() {
        super.draw();
//        renderer.render(world, camera.combined);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

//        if (touchpad.isTouched()){
//            System.out.println(touchpad.getKnobPercentX());
//            if (touchpad.getKnobPercentX() < 0) {
//                player.initMoveLeft();
//            } else if (touchpad.getKnobPercentX() > 0){
//                player.initMoveRight();
//            }
//        } else {
//            player.stopMove();
//        }
        // Fixed timestep
        accumulator += delta;
        camera.update();

        timeFromLastEnemy += delta;
//        if (timeFromLastEnemy > timeToNextEnemy) {
////                createEnemy();
//            timeFromLastEnemy = 0f;
//        }

        if (player.isNeedCreateBullet()) {
            envUtils.createBullet(this, player);
        }

        player.update(delta);
        envUtils.update(delta);

        if (mobsUtils.getCurrBoss() != null) {
            if (mobsUtils.getCurrBoss() instanceof ThinLarry) {
                ThinLarry thinLarry = (ThinLarry) mobsUtils.getCurrBoss();
                if (thinLarry.getLifes() > 0) {
                    thinLarry.update(delta);
                    if (thinLarry.isNeedPoisonBlast()) {
                        thinLarry.setNeedPoisonBlast(false);
                        envUtils.createPoisonBlast(this, thinLarry, false);
                    }
                    if (thinLarry.isNeedCenterPoisonBlast()) {
                        thinLarry.setNeedCenterPoisonBlast(false);
                        envUtils.createPoisonBlast(this, thinLarry, true);
                    }
                } else {
                    thinLarry.setKilledByPlayer(true);
                    thinLarry.getUserData().setNeedDelete(true);
                }
            }
        }
        if (!inTimeout) {
            if (!pauseWorld) {

                if (DiceUtils.throwDices(10, 9)) {
                    envUtils.createFallenBird(this);
                }

                if (gamePlan.isBossState()) {
                    if (gamePlan.isNeedBoss()) {
                        if (bossTimeOut > 0) {
                            bossTimeOut -= delta;
                        } else {
                            parentScreen.getMusicPlayer().changeToBoss();
                            parentScreen.getMusicPlayer().play();
                            mobsUtils.createBoss(MobsUtils.BOSS_LARRY, this, ground, enemyCornerRight);
                            gamePlan.setNeedBoss(false);
                            bossTimeOut = 0;
                        }
                    }
                } else {
                    if (timeFromLastEnemy > gamePlan.getTimeToNextEnemy()) {
                        if (mobsUtils.getMobsCount(Deer.class) < gamePlan.getMaxDeersOnLevel()) {
//                            mobsUtils.createDeer(this, ground);
//                        } else {
                            int result = MathUtils.random(0, 100);
                            if (result == 1) {
                                mobsUtils.createDeer(this, ground);
                                timeFromLastEnemy = 0f;
                            }
//                        }

                        }
                    }
                    if (gamePlan.isNeedRabbit()){
                        if (mobsUtils.getMobsCount(Rabbit.class) < gamePlan.getMaxRabbitsCount()) {
                            mobsUtils.createRabbit(this, ground);
                            gamePlan.setNeedRabbit(false);
                        }
                    }
                }
            }
        } else {
            if (timeout < 0) {
                inTimeout = false;
            } else {
                timeout -= delta;
            }
        }


        mobsUtils.updateWithoutBosses(delta);


        if (player.isDead() && !pauseWorld) {
            stopGameProcess();
            showStopDialog();
        }

        while (accumulator >= delta) {
            world.step(TIME_STEP, 6, 2);
            accumulator -= TIME_STEP;
        }

        mobsUtils.deleteAllMarkedBodies();
        envUtils.deleteAllMarkedBodies();

    }

    @Override
    public boolean keyDown(int keyCode) {
//        if (panel.isVisible()) {
//            reset();
//            panel.hide();
//        } else {
//        envUtils.createFallenBird(this);
//        }
        return true;
    }


    private void showStopDialog() {
    }

    private void stopGameProcess() {
//        world.setContactListener(null);
        parentScreen.getMusicPlayer().stop();
//        parentScreen.getMusicPlayer().changeToBack();
//        parentScreen.getMusicPlayer().play();
        pauseWorld = true;
        envUtils.markAllEnviromentBodiesToDelete();
        mobsUtils.markAllEnemysBodiesToDelete();
//        player.remove();
//        deleteAllMarkedBodies();
//        Iterator<Enemy> iterator = enemies.iterator();
//        while (iterator.hasNext()) {
//            iterator.next().remove();
//            iterator.remove();
//        }
//        reset();
        if (gamePlan.getTotalScore() > bestScore) {
            bestScore = gamePlan.getTotalScore();
        }
        panel.show(gamePlan.getTotalScore(), bestScore);
        gamePlan.sendLeaderBoard();

    }

    private void prepareStopGame() {
        world.dispose();
    }

    private void reset() {
        parentScreen.getMusicPlayer().changeToBack();
        parentScreen.getMusicPlayer().play();
        inTimeout = true;
        timeout = Constants.TIMEOUT_TIME;
//        resetAllDeadEnemies();
        lifeBird.reset();

        gamePlan = new GamePlan(parentScreen.getActionResolver());
        timeFromLastEnemy = 0;

        if (player != null) {
            player.reset();
        }
        updateKillDeerLabel();
        pauseWorld = false;
    }


    private void addDeadBody(Actor actor, Direction direction) {
        DeadBody deadBody = new DeadBody(actor.getX(), actor.getY(), direction);
        addActor(deadBody);
        deadBodies.add(deadBody);
    }


    private void  plusKill(int type) {
        gamePlan.addKill(type);
        updateKillDeerLabel();
        if (gamePlan.isBossState() && gamePlan.isNeedBoss()) {
            bossTimeOut = 3f;
            parentScreen.getMusicPlayer().stop();
            parentScreen.getSoundPlayer().playLarryRoar();

        }
    }

    private void updateKillDeerLabel() {
        killDeerLabel.setText(String.valueOf(gamePlan.getKilledDeerCount()));
        if (gamePlan.getKilledDeerCount() <= 9) {
            killDeerLabel.setX(-133);
        } else if ((gamePlan.getKilledDeerCount() > 9) && ((gamePlan.getKilledDeerCount() <= 99))) {
            killDeerLabel.setX(-144);
        } else {
            killDeerLabel.setX(-157);
        }

    }

    @Override
    public void beginContact(Contact contact) {

        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsGround(b)) ||
                (BodyUtils.bodyIsGround(a) && BodyUtils.bodyIsPlayer(b))) {
            player.landed();
        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsEnemy(b)) ||
                (BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsPlayer(b))) {
            if (!player.isInGodMode()) {
                player.hit();
                parentScreen.getSoundPlayer().playHitDeer();
                lifeBird.flyAway();
            }
        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsRabbit(b)) ||
                (BodyUtils.bodyIsRabbit(a) && BodyUtils.bodyIsPlayer(b))) {
            if (!player.isInGodMode()) {
                player.hit();
                parentScreen.getSoundPlayer().playHitDeer();
                lifeBird.flyAway();
            }
        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsThinLarry(b)) ||
                (BodyUtils.bodyIsThinLarry(a) && BodyUtils.bodyIsPlayer(b))) {
            hitByLarry();
        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsPoisonBlast(b)) ||
                (BodyUtils.bodyIsPoisonBlast(a) && BodyUtils.bodyIsPlayer(b))) {
            hitByLarry();

        } else if ((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsFallenBird(b)) ||
                (BodyUtils.bodyIsFallenBird(a) && BodyUtils.bodyIsPlayer(b))) {

            parentScreen.getSoundPlayer().playBirdPickup();

            if (player.catchBird()) {
                lifeBird.reset();
            } else {
                gamePlan.addPointsForBird();
            }

            if (BodyUtils.bodyIsFallenBird(a)) {
                ((FallenBirdUserData) a.getUserData()).setNeedDelete(true);
            }
            if (BodyUtils.bodyIsFallenBird(b)) {
                ((FallenBirdUserData) b.getUserData()).setNeedDelete(true);
            }

        } else if ((BodyUtils.bodyIsGround(a) && BodyUtils.bodyIsFallenBird(b)) ||
                (BodyUtils.bodyIsFallenBird(a) && BodyUtils.bodyIsGround(b))) {

            parentScreen.getSoundPlayer().playBirdFall();
            FallenLifeBird fallenLifeBird = envUtils.getFallenLifeBird();

            if (fallenLifeBird != null) {
                fallenLifeBird.onGround();
            }

        } else if ((BodyUtils.bodyIsBullet(a) && BodyUtils.bodyIsFallenBird(b)) ||
                (BodyUtils.bodyIsFallenBird(a) && BodyUtils.bodyIsBullet(b))) {

            FallenLifeBird fallenLifeBird = envUtils.getFallenLifeBird();
            if (fallenLifeBird != null) {
                gamePlan.killBird();
            }
        }
        //Пуля - стена
        else if ((BodyUtils.bodyIsBullet(a) && BodyUtils.bodyIsWall(b)) ||
                (BodyUtils.bodyIsWall(a) && BodyUtils.bodyIsBullet(b))) {

            if (BodyUtils.bodyIsBullet(a)) {
                ((BulletUserData) a.getUserData()).setNeedDelete(true);
            }
            if (BodyUtils.bodyIsBullet(b)) {
                ((BulletUserData) b.getUserData()).setNeedDelete(true);
            }
            //Пуля - земля
        } else if ((BodyUtils.bodyIsBullet(a) && BodyUtils.bodyIsGround(b)) ||
                (BodyUtils.bodyIsGround(a) && BodyUtils.bodyIsBullet(b))) {

            if (BodyUtils.bodyIsBullet(a)) {
                ((BulletUserData) a.getUserData()).setNeedDelete(true);
            }
            if (BodyUtils.bodyIsBullet(b)) {
                ((BulletUserData) b.getUserData()).setNeedDelete(true);
            }
            //Пуля - земля
        } else if ((BodyUtils.bodyIsBullet(a) && BodyUtils.bodyIsWorldSensor(b)) ||
                (BodyUtils.bodyIsWorldSensor(a) && BodyUtils.bodyIsBullet(b))) {

            if (BodyUtils.bodyIsBullet(a)) {
                ((BulletUserData) a.getUserData()).setNeedDelete(true);
            }
            if (BodyUtils.bodyIsBullet(b)) {
                ((BulletUserData) b.getUserData()).setNeedDelete(true);
            }

            //Пуля - Ларри
        } else if ((BodyUtils.bodyIsBullet(a) && BodyUtils.bodyIsThinLarry(b)) ||
                (BodyUtils.bodyIsThinLarry(a) && BodyUtils.bodyIsBullet(b))) {

            if (BodyUtils.bodyIsBullet(a)) {
                ((BulletUserData) a.getUserData()).setNeedDelete(true);
            }
            if (BodyUtils.bodyIsBullet(b)) {
                ((BulletUserData) b.getUserData()).setNeedDelete(true);
            }

            if (mobsUtils.getCurrBoss() != null && mobsUtils.getCurrBoss() instanceof ThinLarry) {
                if (BodyUtils.bodyIsThinLarry(a)) {
                    ((ThinLarry) (mobsUtils.getCurrBoss())).hit();
                }
                if (BodyUtils.bodyIsThinLarry(b)) {
                    ((ThinLarry) (mobsUtils.getCurrBoss())).hit();
                }
            }

            //Пуля - тело оленя
        } else if ((BodyUtils.bodyIsBullet(a) && BodyUtils.bodyIsEnemy(b)) ||
                (BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsBullet(b))) {

            if (!((UserData) a.getUserData()).isNeedDelete() && !((UserData) b.getUserData()).isNeedDelete()) {
                plusKill(GamePlan.KILL_TYPE_DEER);
            }

            ((UserData) a.getUserData()).setNeedDelete(true);
            ((UserData) b.getUserData()).setNeedDelete(true);

            //Пуля - Заяц
        } else if ((BodyUtils.bodyIsBullet(a) && BodyUtils.bodyIsRabbit(b)) ||
                (BodyUtils.bodyIsRabbit(a) && BodyUtils.bodyIsBullet(b))) {
            if (!((UserData) a.getUserData()).isNeedDelete() && !((UserData) b.getUserData()).isNeedDelete()) {
                plusKill(GamePlan.KILL_TYPE_RABBIT);
            }

            ((UserData) a.getUserData()).setNeedDelete(true);
            ((UserData) b.getUserData()).setNeedDelete(true);

            //Ларри - Стена
        } else if ((BodyUtils.bodyIsWall(a) && BodyUtils.bodyIsThinLarry(b)) ||
                (BodyUtils.bodyIsThinLarry(a) && BodyUtils.bodyIsWall(b))) {

            if (BodyUtils.bodyIsThinLarry(a)) {
                ((ThinLarryUserData) a.getUserData()).setNeedChangeDirection(true);
            }
            if (BodyUtils.bodyIsThinLarry(b)) {
                ((ThinLarryUserData) b.getUserData()).setNeedChangeDirection(true);
            }

            //Ларри - Центральный сенсор
        } else if ((BodyUtils.bodyIsCenterSensor(a) && BodyUtils.bodyIsThinLarry(b)) ||
                (BodyUtils.bodyIsThinLarry(a) && BodyUtils.bodyIsCenterSensor(b))) {

            if (BodyUtils.bodyIsThinLarry(a) || BodyUtils.bodyIsThinLarry(b)) {
                if (mobsUtils.getCurrBoss() != null && mobsUtils.getCurrBoss() instanceof ThinLarry) {
                    ((ThinLarry) (mobsUtils.getCurrBoss())).setCenterReached(true);
                }
            }
            //Земля - плевок Ларри
        } else if ((BodyUtils.bodyIsPoisonBlast(a) && BodyUtils.bodyIsGround(b)) ||
                (BodyUtils.bodyIsGround(a) && BodyUtils.bodyIsPoisonBlast(b))) {

            if (BodyUtils.bodyIsPoisonBlast(a)) {
                ((PoisonBlastUserData) a.getUserData()).setOnGround(true);
            }
            if (BodyUtils.bodyIsPoisonBlast(b)) {
                ((PoisonBlastUserData) b.getUserData()).setOnGround(true);
            }

            parentScreen.getSoundPlayer().playSplash();

        } else if ((BodyUtils.bodyIsRabbit(a) && BodyUtils.bodyIsGround(b)) ||
                (BodyUtils.bodyIsGround(a)) && BodyUtils.bodyIsRabbit(b)) {
            if (BodyUtils.bodyIsRabbit(a)) {
                ((RabbitUserData) a.getUserData()).setLanded(true);
            }
            if (BodyUtils.bodyIsRabbit(b)) {
                ((RabbitUserData) b.getUserData()).setLanded(true);
            }
        } else if ((BodyUtils.bodyIsRabbit(a) && BodyUtils.bodyIsWall(b)) ||
                (BodyUtils.bodyIsWall(a)) && BodyUtils.bodyIsRabbit(b)) {

            if (BodyUtils.bodyIsRabbit(a)) {
                ((RabbitUserData) a.getUserData()).setNeedChangeDirection(true);
            }
            if (BodyUtils.bodyIsRabbit(b)) {
                ((RabbitUserData) b.getUserData()).setNeedChangeDirection(true);
            }
        }

    }

    @Override
    public void endContact(Contact contact) {

        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();


        if ((BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsWall(b)) ||
                (BodyUtils.bodyIsEnemy(b) && BodyUtils.bodyIsWall(a))) {

            if (BodyUtils.bodyIsEnemy(a)) {
                ((UserData) a.getUserData()).setNeedDelete(true);
            }

            if (BodyUtils.bodyIsEnemy(b)) {
                ((UserData) b.getUserData()).setNeedDelete(true);
            }

        } else if ((BodyUtils.bodyIsRabbit(a) && BodyUtils.bodyIsWall(b)) ||
                (BodyUtils.bodyIsRabbit(b) && BodyUtils.bodyIsWall(a))) {

            if (BodyUtils.bodyIsRabbit(a)) {
                ((UserData) a.getUserData()).setNeedDelete(true);
            }

            if (BodyUtils.bodyIsRabbit(b)) {
                ((UserData) b.getUserData()).setNeedDelete(true);
            }

        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        Body a = contact.getFixtureA().getBody();
        Body b = contact.getFixtureB().getBody();

        if ((BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsEnemy(b)) ||
                (BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsEnemy(b))) {
            contact.setEnabled(false);

        } else if ((BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsWall(b)) ||
                (BodyUtils.bodyIsEnemy(b) && BodyUtils.bodyIsWall(a))) {
            contact.setEnabled(false);

        } else if (((BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsPlayer(b)) ||
                (BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsEnemy(b)))
        ) {
            contact.setEnabled(false);
        } else if (((BodyUtils.bodyIsThinLarry(a) && BodyUtils.bodyIsPlayer(b)) ||
                (BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsThinLarry(b)))
        ) {
            contact.setEnabled(false);
        } else if (BodyUtils.bodyIsPoisonBlast(a) && BodyUtils.bodyIsPoisonBlast(b)) {
            contact.setEnabled(false);
        } else if (((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsPoisonBlast(b)) ||
                (BodyUtils.bodyIsPoisonBlast(a) && BodyUtils.bodyIsPlayer(b)))
        ) {
            contact.setEnabled(false);
        } else if (((BodyUtils.bodyIsThinLarry(a) && BodyUtils.bodyIsPoisonBlast(b)) ||
                (BodyUtils.bodyIsPoisonBlast(a) && BodyUtils.bodyIsThinLarry(b)))
        ) {
            contact.setEnabled(false);
        } else if (BodyUtils.bodyIsCenterSensor(a) || BodyUtils.bodyIsCenterSensor(b)) {
            contact.setEnabled(false);
        } else if (BodyUtils.bodyIsWorldSensor(a) || BodyUtils.bodyIsWorldSensor(b)) {
            contact.setEnabled(false);
        } else if (((BodyUtils.bodyIsThinLarry(a) && BodyUtils.bodyIsEnemy(b)) ||
                (BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsThinLarry(b)))
        ) {
            contact.setEnabled(false);

        } else if (((BodyUtils.bodyIsThinLarry(a) && BodyUtils.bodyIsFallenBird(b)) ||
                (BodyUtils.bodyIsFallenBird(a) && BodyUtils.bodyIsThinLarry(b)))
        ) {
            contact.setEnabled(false);
        } else if (((BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsFallenBird(b)) ||
                (BodyUtils.bodyIsFallenBird(a) && BodyUtils.bodyIsEnemy(b)))
        ) {
            contact.setEnabled(false);
        } else if (((BodyUtils.bodyIsPlayer(a) && BodyUtils.bodyIsFallenBird(b)) ||
                (BodyUtils.bodyIsFallenBird(a) && BodyUtils.bodyIsPlayer(b)))
        ) {
            contact.setEnabled(false);
        } else if (((BodyUtils.bodyIsEnemy(a) && BodyUtils.bodyIsRabbit(b)) ||
                (BodyUtils.bodyIsRabbit(a) && BodyUtils.bodyIsEnemy(b)))
        ) {
            contact.setEnabled(false);
        } else if (((BodyUtils.bodyIsThinLarry(a) && BodyUtils.bodyIsRabbit(b)) ||
                (BodyUtils.bodyIsRabbit(a) && BodyUtils.bodyIsThinLarry(b)))
        ) {
            contact.setEnabled(false);

        } else if (BodyUtils.bodyIsRabbit(a) && BodyUtils.bodyIsRabbit(b))
        {
            contact.setEnabled(false);
        }

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }

    private void hitByLarry() {
        if (!player.isInGodMode()) {
            player.hit();
            parentScreen.getSoundPlayer().playHitDeer();
            lifeBird.flyAway();
//                parentScreen.getSoundPlayer().playSplash();
            parentScreen.getSoundPlayer().playLarryLaught();
        }
    }

    @Override
    public void onTouchDownControlButton(ButtonTypes type, ButtonActionTypes action) {
        switch (type) {
            case PANEL_TYPE:
                switch (action) {
                    case PANEL_NEW_GAME:
                        reset();
                        panel.hide();
                        break;
                    case PANEL_RESET_GAME:
                        prepareStopGame();
                        parentScreen.getScreenCHanger().setTitleScreen();
                        panel.hide();
                        break;
                }
                break;
            case CONTROL_TYPE:
                switch (action) {
                    case LEFT:
                        player.initMoveLeft();
                        break;
                    case RIGHT:
                        player.initMoveRight();
                        break;
                    case JUMP:
                        player.jump();
                        break;
                    case SHOOT:
                        player.setShutting(true);
                        break;

                }
                break;
            case SERVICE_TYPE:
                switch (action) {
                    case BACK:
                        pauseWorld = true;
                        envUtils.markAllEnviromentBodiesToDelete();
                        mobsUtils.markAllEnemysBodiesToDelete();
                        prepareStopGame();
                        parentScreen.getScreenCHanger().setTitleScreen();
                        break;
                }
                break;
        }
    }

    @Override
    public void onTouchUpControlButton(ButtonTypes type, ButtonActionTypes action) {
        switch (action) {
            case LEFT:
                player.stopMoveLeft();
                break;
            case RIGHT:
                player.stopMoveRight();
                break;
        }
    }

    @Override
    public void musicBirdTouched() {

        parentScreen.getMusicPlayer().setEnabled(musicBird.isPlayback());

    }

    @Override
    public void resize(int width, int height) {
        resetButtonsPosition();
    }
}
