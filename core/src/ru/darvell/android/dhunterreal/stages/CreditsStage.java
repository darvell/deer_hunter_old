package ru.darvell.android.dhunterreal.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import ru.darvell.android.dhunterreal.screens.ParentScreen;
import ru.darvell.android.dhunterreal.actors.ui.ButtonActionTypes;
import ru.darvell.android.dhunterreal.actors.ui.ButtonTypes;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;

public class CreditsStage extends NonGameStage implements MyResizibleStage {

    private Skin skin;
    private String creditsValue;
    private Label creditsLabel;

    public CreditsStage(ParentScreen parentScreen) {
        super(parentScreen, Assets.getTexture(Assets.TITLE_BACKGROUND));
        skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        creditsValue = Assets.getBundle(Assets.STRINGS).get(Constants.CREDITS_VALUE);
        initLabels();
        resizeServiceButtons();
    }

    private void initLabels() {
        creditsLabel = new Label(creditsValue, skin);
        addActor(creditsLabel);
        creditsLabel.setX(20);
        creditsLabel.setY(200);
    }

    @Override
    public void handleButtons(ButtonTypes type, ButtonActionTypes action) {

    }

    @Override
    public void resize(int width, int height) {
        resizeServiceButtons();
    }
}
