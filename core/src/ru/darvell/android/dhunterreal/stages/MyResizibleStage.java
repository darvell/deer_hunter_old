package ru.darvell.android.dhunterreal.stages;

public interface MyResizibleStage{

     void resize(int width, int height);
}
