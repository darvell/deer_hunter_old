package ru.darvell.android.dhunterreal.stages;

import ru.darvell.android.dhunterreal.ActionResolver;
import ru.darvell.android.dhunterreal.screens.ParentScreen;
import ru.darvell.android.dhunterreal.PurchasesListener;
import ru.darvell.android.dhunterreal.actors.ui.*;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;

import java.util.List;

public class CatStage extends NonGameStage implements PurchasesListener, MyResizibleStage {

    private Cat cat;
    private PanelCatShop panel;

    public CatStage(ParentScreen parentScreen) {
        super(parentScreen, Assets.getTexture(Assets.CAT_SCREEN_BACKGROUND));

        cat = new Cat(Constants.SAD_CAT_POSITION.x, Constants.SAD_CAT_POSITION.y);
        addActor(cat);
        initButtons();
        initPanel();
    }

    private void initPanel() {
        panel = new PanelCatShop(Constants.CAT_SHOP_PANEL_POSITION.x, Constants.CAT_SHOP_PANEL_POSITION.y, this);
        addActor(panel);
        panel.setVisible(false);
    }

    private void initButtons() {
        addActor(ButtonFabric.getButton(780f, 540f, ButtonTypes.TITLE_TYPE, ButtonActionTypes.FEED, this));

    }

    @Override
    public void handleButtons(ButtonTypes type, ButtonActionTypes action) {
        switch (type) {
            case TITLE_TYPE:
                switch (action) {
                    case FEED:
                        ActionResolver ar = parentScreen.getActionResolver();
                        if (ar != null) {
                            panel.refresh(ar.getSkus());
                        }
                        panel.setVisible(true);
                        break;
                }
                break;
            case CAT_SHOP:
                switch (action) {
                    case BUY_BEER:
                        doPurchase(Constants.SKU_BEER);
                        break;
                    case BUY_SALAD:
                        doPurchase(Constants.SKU_SALAD);
                        break;
                    case BUY_MEAT:
                        doPurchase(Constants.SKU_MEAT);
                        break;
                }
                break;
        }
    }

    @Override
    public void tochDownBackground() {
        if (panel.isVisible()) {
            panel.setVisible(false);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    @Override
    public void onGetPurchases(List<String> purchasesIds) {

    }

    private void doPurchase(String skuId) {
        ActionResolver actionResolver = parentScreen.getActionResolver();
        if (actionResolver != null) {
            actionResolver.purchaseItem(skuId);
        }
    }

    @Override
    public void resize(int width, int height) {
        resizeServiceButtons();
    }
}
