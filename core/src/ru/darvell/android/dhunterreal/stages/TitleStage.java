package ru.darvell.android.dhunterreal.stages;

import ru.darvell.android.dhunterreal.screens.ParentScreen;
import ru.darvell.android.dhunterreal.actors.ui.Button;
import ru.darvell.android.dhunterreal.actors.ui.ButtonActionTypes;
import ru.darvell.android.dhunterreal.actors.ui.ButtonFabric;
import ru.darvell.android.dhunterreal.actors.ui.ButtonTypes;
import ru.darvell.android.dhunterreal.utils.Assets;
import ru.darvell.android.dhunterreal.utils.Constants;

public class TitleStage extends NonGameStage implements MyResizibleStage {

    public static final String TAG = "TitleStage";

    public TitleStage(ParentScreen parentScreen) {
        super(parentScreen,  Assets.getTexture(Assets.TITLE_BACKGROUND));
        System.out.println("1");
        initButtons();

    }


    private void initButtons(){
        Button btn = ButtonFabric.getButton(102f, 554f, ButtonTypes.TITLE_TYPE, ButtonActionTypes.NEW_GAME, this);
        addActor(btn);


        Button btn1 = ButtonFabric.getButton(102f, btn.getY() - Constants.BUTTON_TITLE_HEIGHT - 40f, ButtonTypes.TITLE_TYPE, ButtonActionTypes.CREDITS, this);
        addActor(btn1);

//        Button btn1 = ButtonFabric.getButton(102f, btn.getY() - Constants.BUTTON_TITLE_HEIGHT - 40f, ButtonTypes.TITLE_TYPE, ButtonActionTypes.CAT, this);
//        addActor(btn1);
//
//        Button btn2 = ButtonFabric.getButton(102f, btn1.getY() - Constants.BUTTON_TITLE_HEIGHT - 40f, ButtonTypes.TITLE_TYPE, ButtonActionTypes.CREDITS, this);
//        addActor(btn2);

    }

    @Override
    public void handleButtons(ButtonTypes type, ButtonActionTypes action) {
        switch (type) {
            case TITLE_TYPE:
                switch (action) {
                    case NEW_GAME:
                        parentScreen.getMusicPlayer().stop();
                        parentScreen.getScreenCHanger().setPlayScreen();
                        break;
                    case CREDITS:
                        parentScreen.getScreenCHanger().setCreditsScreen();
                        break;
                    case CAT:
                        parentScreen.getScreenCHanger().setCatScreen();
                        break;
                }
        }
    }


    @Override
    public void resize(int width, int height) {
        resizeServiceButtons();
    }
}
