package ru.darvell.android.dhunterreal.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import ru.darvell.android.dhunterreal.BackgroundClickListener;
import ru.darvell.android.dhunterreal.DHunterReal;
import ru.darvell.android.dhunterreal.actors.Background;
import ru.darvell.android.dhunterreal.actors.ui.*;
import ru.darvell.android.dhunterreal.screens.ParentScreen;
import ru.darvell.android.dhunterreal.utils.Constants;
import ru.darvell.android.dhunterreal.utils.GdxLog;

public abstract class NonGameStage extends Stage implements MyButtonListener, BackgroundClickListener{

    public static final String TAG = "NonGameStage";

    private static final int VIEWPORT_WIDTH = Constants.APP_WIDTH;
    protected ParentScreen parentScreen;
    protected float maxY;
    protected float minY;
    private OrthographicCamera camera;
    private Button musicButton;
    private Button backButton;

    public NonGameStage(ParentScreen parentScreen, TextureRegion background) {
        super(new FillViewport(Constants.APP_WIDTH, Constants.APP_HEIGHT));
        setupCamera();
        getViewport().setCamera(camera);
        this.parentScreen = parentScreen;
        setUpBackground(background);
//        maxY = GraphicsUtils.calculateUpperY();
//        minY = GraphicsUtils.calculateButtomY();
        resizeServiceButtons();

        GdxLog.d(TAG, "Stage width: " + getWidth());
        GdxLog.d(TAG, "Stage height: " + getHeight());
        GdxLog.d(TAG, "Screen height: " + Gdx.graphics.getHeight());
    }

    private void setupCamera() {
        camera = new OrthographicCamera(VIEWPORT_WIDTH, Gdx.graphics.getHeight());
        camera.position.set(Constants.APP_WIDTH / 2, Constants.APP_HEIGHT / 2, 0f);
        camera.update();
    }

    private void setUpBackground(TextureRegion backgroundTexture) {
        Background background = new Background(0, 0, backgroundTexture, this);
        addActor(background);
    }

    protected void resizeServiceButtons(){
        setupBackButton();
        resetMusicButton(parentScreen.getMusicPlayer().isEnabled());
    }

    private void resetMusicButton(boolean musicEnabled) {
        if (musicButton != null) {
            musicButton.remove();
            musicButton = null;
        }
        if (musicEnabled) {
            musicButton = ButtonFabric.getButton(getWidth() - Constants.BUTTON_PANEL_WIDTH - 20, DHunterReal.MAX_Y - Constants.BUTTON_PANEL_HEIGHT - 20, ButtonTypes.SERVICE_TYPE, ButtonActionTypes.MUSIC_ON, this);
        } else {
            musicButton = ButtonFabric.getButton(getWidth() - Constants.BUTTON_PANEL_WIDTH - 20, DHunterReal.MAX_Y - Constants.BUTTON_PANEL_HEIGHT - 20, ButtonTypes.SERVICE_TYPE, ButtonActionTypes.MUSIC_OFF, this);
        }
        addActor(musicButton);
    }

    private void setupBackButton() {
        if (backButton == null) {
            backButton = ButtonFabric.getButton(getWidth() - Constants.BUTTON_PANEL_WIDTH - 20, DHunterReal.MIN_Y + 20, ButtonTypes.SERVICE_TYPE, ButtonActionTypes.BACK, this);
            addActor(backButton);
        } else {
            backButton.setX(getWidth() - Constants.BUTTON_PANEL_WIDTH - 20);
            backButton.setY(DHunterReal.MIN_Y + 20);
        }
    }

    public abstract void handleButtons(ButtonTypes type, ButtonActionTypes action);

    @Override
    public void onTouchDownControlButton(ButtonTypes buttonTypes, ButtonActionTypes type) {

    }

    @Override
    public final void onTouchUpControlButton(ButtonTypes type, ButtonActionTypes action) {
        switch (type) {

            case SERVICE_TYPE:
                switch (action) {
                    case MUSIC_ON:
                        parentScreen.getMusicPlayer().setEnabled(false);
                        resetMusicButton(parentScreen.getMusicPlayer().isEnabled());
                        break;
                    case MUSIC_OFF:
                        parentScreen.getMusicPlayer().setEnabled(true);
                        resetMusicButton(parentScreen.getMusicPlayer().isEnabled());
                        break;
                    case BACK:
                        if (this instanceof TitleStage){
                            Gdx.app.exit();
                        } else {
                            parentScreen.getScreenCHanger().setTitleScreen();
                        }
                        break;
                }
                break;
            default:
                handleButtons(type, action);
        }
    }

    @Override
    public void tochDownBackground() {

    }


}
