package ru.darvell.android.dhunterreal.enums;

public enum UserDataType {

    GROUND,
    PLAYER,
    ENEMY,
    BULLET,
    WALL,
    WORLD_SENSOR,
    CENTER_SENSOR,
    THIN_LARRY,
    POISON_BLAST,
    FALLEN_BIRD,
    RABBIT

}
