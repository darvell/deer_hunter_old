package ru.darvell.android.dhunterreal.enums;

public enum Direction {

    RIGHT,
    LEFT
}
