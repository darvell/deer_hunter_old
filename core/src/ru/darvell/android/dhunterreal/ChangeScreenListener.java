package ru.darvell.android.dhunterreal;

public interface ChangeScreenListener {

    void setPlayScreen();
    void setTitleScreen();
    void setCreditsScreen();
    void setCatScreen();


}
