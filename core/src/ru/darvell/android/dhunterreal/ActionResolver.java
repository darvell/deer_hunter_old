package ru.darvell.android.dhunterreal;

import java.util.List;

public interface ActionResolver {

    void onSignInButtonClicked();
    void onSignOutButtonClicked();
    boolean isSignedIn();
    void signInSilently();
    void submitScore(String leaderboardId, int highScore);
    void showLeaderboard(String leaderboardId);
    void setTrackerScreenName(String screenName);
    void unlockAchievement(String achievmentId);

    List<MySkuDetails> getSkus();
    void purchaseItem(String skuId);
    void setupListener(PurchasesListener purchasesListener);
    void disableListener();

    //http://amnelgames.blogspot.ru/2018/03/game-services-firebase-in-libgdx-using.html
    //https://github.com/playgameservices/android-basic-samples/blob/master/TypeANumber/src/main/java/com/google/example/games/tanc/MainActivity.java
}
