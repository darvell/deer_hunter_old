package ru.darvell.android.dhunterreal;

public class MySkuDetails {
    private String skuName;
    private String price;

    public MySkuDetails(String skuName, String price) {
        this.skuName = skuName;
        this.price = price;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
