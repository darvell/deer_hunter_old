package ru.darvell.android.dhunterreal;

import java.util.List;

public interface PurchasesListener {
    void onGetPurchases(List<String> purchasesIds);
}
