package ru.darvell.android.dhunterreal.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import ru.darvell.android.dhunterreal.ActionResolver;
import ru.darvell.android.dhunterreal.ChangeScreenListener;
import ru.darvell.android.dhunterreal.PurchasesListener;
import ru.darvell.android.dhunterreal.stages.CatStage;
import ru.darvell.android.dhunterreal.utils.GdxLog;
import ru.darvell.android.dhunterreal.utils.MusicPlayer;
import ru.darvell.android.dhunterreal.utils.SoundPlayer;

public class CatScreen extends MyScreen{

    public static final String TAG = "CatScreen";


    public CatScreen(ChangeScreenListener screenChanger, ActionResolver actionResolver, MusicPlayer musicPlayer, SoundPlayer soundPlayer) {
        super(screenChanger, actionResolver, musicPlayer, soundPlayer);
        stage = new CatStage(this);
        Gdx.input.setInputProcessor(stage);
        if (getActionResolver() != null) {
            getActionResolver().setupListener((PurchasesListener) stage);
        }
    }

    @Override
    public void show() {
        GdxLog.d(TAG, "show()");
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
        stage.act(delta);
    }



    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        if (getActionResolver() != null) {
            getActionResolver().disableListener();
        }
    }
}
