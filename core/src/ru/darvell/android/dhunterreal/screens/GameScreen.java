package ru.darvell.android.dhunterreal.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import ru.darvell.android.dhunterreal.ActionResolver;
import ru.darvell.android.dhunterreal.ChangeScreenListener;
import ru.darvell.android.dhunterreal.stages.OldGameStage;
import ru.darvell.android.dhunterreal.stages.MyResizibleStage;
import ru.darvell.android.dhunterreal.utils.GdxLog;
import ru.darvell.android.dhunterreal.utils.MusicPlayer;
import ru.darvell.android.dhunterreal.utils.SoundPlayer;

public class GameScreen extends MyScreen{

    private static final String TAG = "GameScreen";

    private Stage stage;

    public GameScreen(ChangeScreenListener screenChanger, ActionResolver actionResolver, MusicPlayer musicPlayer, SoundPlayer soundPlayer) {
        super(screenChanger, actionResolver, musicPlayer, soundPlayer);
        stage = new OldGameStage(this);
//        stage = new GameStage(new FillViewport(Constants.APP_WIDTH, Constants.APP_HEIGHT));
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void show() {
        GdxLog.d(TAG, "show()");
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
        stage.act(delta);
    }

    @Override
    public void resize(int width, int height) {
        GdxLog.d(TAG, "resize()");
        stage.getViewport().update(width, height);
        if (stage != null && stage instanceof MyResizibleStage){
            ((MyResizibleStage)stage).resize(width, height);
        }
    }

    @Override
    public void pause() {
        GdxLog.d(TAG, "pause()");
    }

    @Override
    public void resume() {
        GdxLog.d(TAG, "resume()");
    }

    @Override
    public void hide() {
        GdxLog.d(TAG, "hide()");

    }

    @Override
    public void dispose() {
        GdxLog.d(TAG, "dispose()");
        stage.dispose();
    }

}
