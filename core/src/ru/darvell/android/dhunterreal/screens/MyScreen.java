package ru.darvell.android.dhunterreal.screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.scenes.scene2d.Stage;
import ru.darvell.android.dhunterreal.ActionResolver;
import ru.darvell.android.dhunterreal.ChangeScreenListener;
import ru.darvell.android.dhunterreal.stages.MyResizibleStage;
import ru.darvell.android.dhunterreal.utils.MusicPlayer;
import ru.darvell.android.dhunterreal.utils.SoundPlayer;

public abstract class MyScreen implements Screen, ParentScreen {

    private ChangeScreenListener screenChanger;
    private ActionResolver actionResolver;
    private MusicPlayer musicPlayer;
    private SoundPlayer soundPlayer;
    protected Stage stage;

    public MyScreen(ChangeScreenListener screenChanger, ActionResolver actionResolver, MusicPlayer musicPlayer, SoundPlayer soundPlayer){
        this.screenChanger = screenChanger;
        this.actionResolver = actionResolver;
        this.musicPlayer = musicPlayer;
        this.soundPlayer = soundPlayer;
    }


    @Override
    public ChangeScreenListener getScreenCHanger() {
        return screenChanger;
    }

    @Override
    public ActionResolver getActionResolver() {
        return actionResolver;
    }

    @Override
    public MusicPlayer getMusicPlayer() {
        return musicPlayer;
    }

    @Override
    public SoundPlayer getSoundPlayer() {
        return soundPlayer;
    }

    public void setStage(Stage stage){
        this.stage = stage;
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height);
        if (stage != null && stage instanceof MyResizibleStage){
            ((MyResizibleStage)stage).resize(width, height);
        }
    }
}
