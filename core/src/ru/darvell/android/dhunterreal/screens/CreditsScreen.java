package ru.darvell.android.dhunterreal.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import ru.darvell.android.dhunterreal.ActionResolver;
import ru.darvell.android.dhunterreal.ChangeScreenListener;
import ru.darvell.android.dhunterreal.stages.CreditsStage;
import ru.darvell.android.dhunterreal.utils.MusicPlayer;
import ru.darvell.android.dhunterreal.utils.SoundPlayer;

public class CreditsScreen extends MyScreen {


    public CreditsScreen(ChangeScreenListener screenChanger, ActionResolver actionResolver, MusicPlayer musicPlayer, SoundPlayer soundPlayer) {
        super(screenChanger, actionResolver, musicPlayer, soundPlayer);
        stage = new CreditsStage(this);
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {
        getMusicPlayer().changeToTitle();
        getMusicPlayer().play();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
        stage.act(delta);
    }



    @Override
    public void pause() {
        getMusicPlayer().pause();
    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
