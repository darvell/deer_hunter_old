package ru.darvell.android.dhunterreal.screens;

import ru.darvell.android.dhunterreal.ActionResolver;
import ru.darvell.android.dhunterreal.ChangeScreenListener;
import ru.darvell.android.dhunterreal.utils.MusicPlayer;
import ru.darvell.android.dhunterreal.utils.SoundPlayer;

public interface ParentScreen {

    ChangeScreenListener getScreenCHanger();
    ActionResolver getActionResolver();
    MusicPlayer getMusicPlayer();
    SoundPlayer getSoundPlayer();

}
