package ru.darvell.android.dhunterreal.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import ru.darvell.android.dhunterreal.DHunterReal;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//		config.width = Constants.APP_WIDTH;
//		config.height = Constants.APP_HEIGHT;
		config.width = 1024;
		config.height = 600;
		new LwjglApplication(new DHunterReal(null), config);
	}
}
