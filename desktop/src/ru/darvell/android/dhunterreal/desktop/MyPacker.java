package ru.darvell.android.dhunterreal.desktop;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class MyPacker {
    public static void main (String[] args) throws Exception {
        TexturePacker.process("imagesPng/"
                , "android/assets/data/game_atlas", "atlas");
    }
}
