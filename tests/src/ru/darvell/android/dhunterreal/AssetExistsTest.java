package ru.darvell.android.dhunterreal;

import com.badlogic.gdx.Gdx;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

@RunWith(GdxTestRunner.class)
public class AssetExistsTest {

    @Test
    public void checkAssets(){
        assertTrue(Gdx.files.internal("../android/assets/data/atlas.png").exists());
    }

}
